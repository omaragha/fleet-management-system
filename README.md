# (fleet-management-system) project

## Installation

### Create your .env File
> you can copy and paste .env.example file ;)

### Install packages
```bash
$ composer install
```

### Generate Application Key
```bash
$ php artisan key:generate
```

### Link storage
```bash
$ php artisan storage:link
```

### Migrate Database
```bash
$ php artisan migrate
```

### Install passport keys
```bash
$ php artisan passport:install
```
> This command will create the encryption keys needed to generate secure access tokens. In addition, the command will create "personal access" and "password grant" clients which will be used to generate access tokens.

### Set PASSPORT_CLIENT_SECRET 
```
goto .env file and edit 'PASSPORT_CLIENT_SECRET' value.
this value should be in DB in table 'oauth_clients' value of (Laravel Password Grant Client)
```
or import it from sql serve.

### Seeding Database
```bash
php artisan db:seed
```
------------
Admin account
**username**: admin@gmail.com

**password**: 123456
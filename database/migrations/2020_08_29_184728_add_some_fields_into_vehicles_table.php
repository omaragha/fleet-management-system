<?php

use App\Enums\VehicleStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldsIntoVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->string('serial_number')->unique();
            $table->string('license_plate')->nullable();
            $table->timestamp('year')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->enum('status', ['active', 'out-of-service', 'in-active'])->default(VehicleStatus::toArray()[2]);
            $table->string('color')->nullable();
            $table->timestamp('in_service_date')->nullable();
            $table->string('in_service_odmeter')->nullable();
            $table->string('estimated_service_life_in_month')->nullable();
            $table->string('estimated_service_life_in_mile')->nullable();
            $table->timestamp('out_of_service_date')->nullable();
            $table->string('out_of_service_odemeter')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('length')->nullable();
            $table->integer('passenger_volume')->nullable();
            $table->integer('empty_weight')->nullable();
            $table->integer('full_weight')->nullable();
            $table->string('engine_summary')->nullable();
            $table->string('enigine_brand')->nullable();
            $table->string('aspiration')->nullable();
            $table->string('block_type')->nullable();
            $table->integer('valves')->nullable();
            $table->string('drive_type')->nullable();
            $table->string('brake_system')->nullable();
            $table->integer('oil_capacity')->nullable();
            $table->string('fuel_type')->nullable();
            $table->integer('fuel_tank_capacity')->nullable();
            $table->bigInteger('group_id')->unsigned()->index();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            // $table->bigInteger('id')->primary('id')->change();
            // $table->dropColumn([
            //     'serial_number',
            //     'license_plate',
            //     'year',
            //     'make',
            //     'model',
            //     'status',
            //     'color',
            //     'in_service_date',
            //     'estimated_service_life_in_month',
            //     'estimated_service_life_in_mile',
            //     'out_of_service_date,',
            //     'out_of_service_odemeter',
            //     'width',
            //     'height',
            //     'length',
            //     'passenger_volume',
            //     'empty_weight',
            //     'full_weight',
            //     'engine_summary',
            //     'enigine_brand',
            //     'aspiration',
            //     'block_type',
            //     'valves',
            //     'drive_type',
            //     'brake_system',
            //     'oil_capacity',
            //     'fuel_type',
            //     'fuel_tank_capacity'
            // ]);
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyVehicleModelIdFieldToUserChecklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_checklist', function (Blueprint $table) {
            $table->bigInteger('company_vehicle_model_id')->unsigned()->index()->nullable();
            $table->foreign('company_vehicle_model_id')->references('id')->on('company_vehicle_model')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_checklist', function (Blueprint $table) {
            $table->dropColumn('company_vehicle_model_id');
        });
    }
}

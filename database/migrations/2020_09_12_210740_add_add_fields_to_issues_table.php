<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddFieldsToIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issues', function (Blueprint $table) {
            //

            $table->string('end_date')->nullable();
            $table->string('deadline')->nullable();
            $table->string('image')->nullable();
            $table->double('odometer')->nullable();
            $table->enum('status',['pending','running','closed','solved'])->default('pending');
            $table->enum('priority',['hight','moderate','low'])->default('moderate');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issues', function (Blueprint $table) {
            
            $table->dropColumn('end_date');
            $table->dropColumn('deadline');
            $table->dropColumn('image');
            $table->dropColumn('priority');
            $table->dropColumn('odometer');
            $table->dropColumn('status');
            $table->dropColumn('priority');

        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecklistServiceProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_service_program', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('service_program_id')->unsigned()->index();
            $table->foreign('service_program_id')->references('id')->on('service_programs')->onDelete('cascade');
            $table->bigInteger('checklist_id')->unsigned()->index();
            $table->foreign('checklist_id')->references('id')->on('checklists')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('vehicle_id')->unsigned()->index();
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_service_program');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriggerSparepartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::unprepared('
        
        //     CREATE TRIGGER delete_visit_attribute ON spareparts
        //     INSTEAD OF DELETE
        //     AS
        //     BEGIN
        //         declare @id int;
        //         select @id = id from deleted;
        //         delete from visit_attribute where visit_attribute.sparepart_id = @id;
        //         delete from spareparts where id = @id;
        //     END
        
        // ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER [dbo].[delete_visit_attribute];');
    }
}

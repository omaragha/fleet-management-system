<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTriggerInCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::unprepared('
        
        //     CREATE TRIGGER delete_vehilces ON companies
        //     INSTEAD OF DELETE
        //     AS
        //     BEGIN
        //         declare @id int;
        //         select @id = id from deleted;
        //         delete from vehicles where vehicles.company_id = @id;
        //         delete from companies where id = @id;
        //     END
        
        // ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER [dbo].[delete_vehilces];');
    }
}

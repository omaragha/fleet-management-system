<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldsToProblemReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('problem_reports', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->enum('status', ['acknowledged', 'pending', 'checked'])->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('problem_reports', function (Blueprint $table) {
            $table->dropColumn(['name', 'image', 'status']);
        });
    }
}

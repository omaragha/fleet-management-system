<?php

use App\Enums\ItemFailureType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemFailuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_failures', function (Blueprint $table) {
            $table->id();
            $table->text('justification')->nullable();
            $table->enum('status',['closed','acknowledged','pending'])->default('pending');
            $table->bigInteger('visit_attribute_id')->unsigned()->index();
            $table->foreign('visit_attribute_id')->references('id')->on('visit_attribute')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_failures');
    }
}

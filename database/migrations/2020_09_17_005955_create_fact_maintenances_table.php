<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::connection('starschema')->create('fact_maintenances', function (Blueprint $table) {
        //     $table->id();
        //     $table->bigInteger('dim_vehicle_id')->unsigned()->index()->nullable();
        //     $table->foreign('dim_vehicle_id')->references('id')->on('dim_vehicles');
        //     $table->bigInteger('dim_date_id')->unsigned()->index()->nullable();
        //     $table->foreign('dim_date_id')->references('id')->on('dim_dates');
        //     $table->bigInteger('dim_model_id')->unsigned()->index()->nullable();
        //     $table->foreign('dim_model_id')->references('id')->on('dim_models');
        //     $table->bigInteger('dim_sparepart_id')->unsigned()->index()->nullable();
        //     $table->foreign('dim_sparepart_id')->references('id')->on('dim_spareparts');
        //     $table->bigInteger('dim_sparepart_type_id')->unsigned()->index()->nullable();
        //     $table->foreign('dim_sparepart_type_id')->references('id')->on('dim_sparepart_types');
        //     $table->bigInteger('dim_company_id')->unsigned()->index()->nullable();
        //     $table->foreign('dim_company_id')->references('id')->on('dim_companies');
        //     $table->double('count')->nullable();
        //     $table->double('cost')->nullable();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('starschema')->dropIfExists('fact_maintenances');
    }
}

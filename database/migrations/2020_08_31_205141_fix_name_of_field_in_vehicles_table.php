<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixNameOfFieldInVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            if (Schema::hasColumn('vehicles', 'out_of_service_odemeter')) {
                $table->renameColumn('out_of_service_odemeter', 'out_of_service_odmeter');
            }
            if (Schema::hasColumn('vehicles', 'enigine_brand')) {
                $table->renameColumn('enigine_brand', 'engine_brand');
            }

            $table->float('width')->nullable()->change();
            $table->float('length')->nullable()->change();
            $table->float('height')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            if (Schema::hasColumn('vehicles', 'out_of_service_odmeter')) {
                $table->renameColumn('out_of_service_odmeter', 'out_of_service_odemeter');
            }
            if (Schema::hasColumn('vehicles', 'engine_brand')) {
                $table->renameColumn('engine_brand', 'enigine_brand');
            }
            $table->integer('width')->nullable()->change();
            $table->integer('length')->nullable()->change();
            $table->integer('height')->nullable()->change();
        });
    }
}

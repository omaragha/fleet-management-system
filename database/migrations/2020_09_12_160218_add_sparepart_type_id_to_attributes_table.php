<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSparepartTypeIdToAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attributes', function (Blueprint $table) {
            $table->bigInteger('sparepart_type_id')->unsigned()->index()->nullable();
            $table->foreign('sparepart_type_id')->references('id')->on('sparepart_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attributes', function (Blueprint $table) {
            if (Schema::hasColumn('attributes', 'sparepart_type_id')) {
                $table->dropColumn(['sparepart_type_id']);
            }
        });
    }
}

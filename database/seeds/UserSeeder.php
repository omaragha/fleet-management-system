<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('sparepart_types')->insert([
            'name' => 'oil',
            'description' => 'oil information.'
        ]);
        DB::table('sparepart_types')->insert([
            'name' => 'fuel',
            'description' => 'fuel information.'
        ]);

        $inputs = ['text','number','checkbox','twoCheckbox','select','multiSelect'];
        $operations = ['<','=','>','<=','>=','between'];

        $checklistTypes = ['PM','CM','CF'];


        for ($i = 0 ; $i < count($inputs) ; $i++) {
            DB::table('input_types')->insert([
                'name'=>$inputs[$i]
            ]);
        }

        for ($i = 0 ; $i < count($operations) ; $i++) {
            DB::table('operations')->insert([
                'operation'=>$operations[$i]
            ]);
        }

        for ($i = 0 ; $i < count($checklistTypes) ; $i++) {
            DB::table('checklist_types')->insert([
                'name'=>$checklistTypes[$i]
            ]);
        }


      DB::table('roles')->insert([
            'name' => 'admin',
            'description' => 'some description.'
        ]);

        DB::table('roles')->insert([
            'name' => 'driver',
            'description' => 'some description.'
        ]);

        DB::table('roles')->insert([
            'name' => 'technical',
            'description' => 'some description.'
        ]);

        DB::table('packages')->insert([
            'name'     => '1-20',
            'max_size' => '20',
            'min_size' => '1'
        ]);

        DB::table('packages')->insert([
            'name'     => '1-10',
            'max_size' => '10',
            'min_size' => '1'
        ]);

        DB::table('packages')->insert([
            'name'     => '1-30',
            'max_size' => '30',
            'min_size' => '1'
        ]);
        DB::table('packages')->insert([
            'name'     => '1-50',
            'max_size' => '50',
            'min_size' => '1'
        ]);
        DB::table('packages')->insert([
            'name'     => '1-100',
            'max_size' => '100',
            'min_size' => '1'
        ]);

        DB::table('companies')->insert([
            'package_id' => DB::table('packages')->first()->id,
            'name' => 'fighters',
            'email'=> 'abdulfattah.khudari@gmail.com',
            'location' => 'Damascues,Syria',
            'phone' =>'0952432706',
            'field' => 1
        ]);



        DB::table('users')->insert([
            'first_name' => 'admin',
            'last_name'  => 'admin',
            'email'      => 'admin@gmail.com',
            'password'   => bcrypt('123456'),
            'role_id'    => DB::table('roles')->where('id', 1)->get()->first()->id,
            'company_id' => DB::table('companies')->orderBy('id', 'DESC')->limit(1)->get()->first()->id
        ]);

        DB::table('users')->insert([
            'first_name' => 'driver',
            'last_name'  => 'driver',
            'email'      => 'driver@gmail.com',
            'password'   => bcrypt('123456'),
            'role_id'    => DB::table('roles')->where('id', 2)->get()->first()->id,
            'company_id' => DB::table('companies')->orderBy('id', 'DESC')->limit(1)->get()->first()->id
        ]);

        DB::table('users')->insert([
            'first_name' => 'technical',
            'last_name'  => 'technical',
            'email'      => 'technical@gmail.com',
            'password'   => bcrypt('123456'),
            'role_id'    => DB::table('roles')->where('id', 3)->get()->first()->id,
            'company_id' => DB::table('companies')->orderBy('id', 'DESC')->limit(1)->get()->first()->id
        ]);

        DB::table('main_vehicle_models')->insert([
            'name' => 'Peagaut'
        ]);

        DB::table('vehicle_models')->insert([
            'name' => '207',
            'type' => '207',
            'main_vehicle_model_id' => DB::table('main_vehicle_models')->get()->first()->id
        ]);

        DB::table('company_vehicle_model')->insert([
            'vehicle_model_id' => DB::table('vehicle_models')->get()->first()->id,
            'company_id' => DB::table('companies')->orderBy('id', 'DESC')->limit(1)->get()->first()->id
        ]);

        DB::table('groups')->insert([
            'name' => 'Vehicle'
        ]);


    }
}

<?php

use Illuminate\Database\Seeder;

class JustificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $justifications = [
            "Hot area",
            "Out-of-service",
            "Car not exsit",
            "Others"
        ];

        foreach ($justifications as $key => $value) {
            DB::table('justifications')->insert([
                'name'=>$value
            ]);
        }
    }
}

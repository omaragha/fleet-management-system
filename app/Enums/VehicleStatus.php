<?php

namespace App\Enums;

class VehicleStatus {

    const active = 1;
    const outOfService = 2;
    const inActive = 3;

    public static function toArray() {
        return [
            'active',
            'out-of-service',
            'in-active'
        ];
    }
}

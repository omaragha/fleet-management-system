<?php

namespace App\Enums;

class ServiceProgramType {

    const daily = 1;
    const quarter = 2;
    const weekly = 3;
    const annual = 4;
    const yearly = 5;
    const monthly = 6;

    public static function toArray() {
        return [
            'daily',
            'quarter',
            'weekly',
            'annual',
            'yearly',
            'monthly'
        ];
    }
}

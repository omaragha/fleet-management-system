<?php

namespace App\Enums;

class ItemFailureType {

    const achnowledged = 1;
    const closed = 2;
    const solved = 3;

    public static function toArray() {
        return [
            'achnowledged',
            'closed',
            'solved'
        ];
    }
}

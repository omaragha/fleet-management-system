<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProblemReport extends Model
{

    protected $fillable = [
        'description',
        'user_id',
        'sparepart_type_id',
        'vehicle_id',
        'name',
        'image',
        'status'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function sparepartType() {
        return $this->belongsTo('App\Models\SparepartType');
    }

    public function vehicle() {
        return $this->belongsTo('App\Models\Vehicle');
    }

}

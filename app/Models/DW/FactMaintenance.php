<?php

namespace App\Models\DW;

use Illuminate\Database\Eloquent\Model;

class FactMaintenance extends Model
{

    protected $connection = 'starschema';

    protected $fillable = [
        'dim_date_id',
        'dim_company_id',
        'dim_vehicle_id',
        'dim_model_id',
        'dim_sparepart_type_id',
        'dim_sparepart_id',
        'cost',
        'count'
    ];

}

<?php

namespace App\Models\DW;

use Illuminate\Database\Eloquent\Model;

class DimSparepart extends Model
{

    protected $connection = 'starschema';

    protected $fillable = ['name', 'code'];

}

<?php

namespace App\Models\DW;

use Illuminate\Database\Eloquent\Model;

class DimVehicle extends Model
{

    protected $connection = 'starschema';

    protected $fillable = ['name', 'serial_number'];

}

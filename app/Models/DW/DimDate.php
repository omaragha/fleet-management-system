<?php

namespace App\Models\DW;

use Illuminate\Database\Eloquent\Model;

class DimDate extends Model
{

    protected $connection = 'starschema';

    protected $fillable = ['month', 'year'];

}

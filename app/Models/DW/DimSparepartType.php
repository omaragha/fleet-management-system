<?php

namespace App\Models\DW;

use Illuminate\Database\Eloquent\Model;

class DimSparepartType extends Model
{

    protected $connection = 'starschema';

    protected $fillable = ['name'];

}

<?php

namespace App\Models\DW;

use Illuminate\Database\Eloquent\Model;

class DimModel extends Model
{

    protected $connection = 'starschema';

    protected $fillable = ['name', 'main_model'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{

    protected $fillable = [
        'name',
        'type',
        'main_vehicle_model_id'
    ];

    public function mainVehicleModel() {
        return $this->belongsTo('App\Models\MainVehicleModel');
    }

    public function vehicles() {
        return $this->hasMany('App\Models\Vehicle');
    }

    public function companies() {
        return $this->belongsToMany('App\Models\Company')->withPivot('id');
    }

}

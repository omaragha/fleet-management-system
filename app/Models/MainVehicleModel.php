<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainVehicleModel extends Model
{

    protected $fillable = ['name'];

    public function vehicleModels() {
        return $this->hasMany('App\Models\VehicleModel');
    }

}

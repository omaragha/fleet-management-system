<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProgram extends Model
{

    protected $fillable = ['name', 'description', 'company_id', 'type'];


    public function programVisits() {
        return $this->hasMany('App\Models\ProgramVisit');
    }

}

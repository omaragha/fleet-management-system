<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    
    protected $fillable = ['attribute_id', 'operation_id', 'value'];

    public function operation() {
        return $this->belongsTo('App\Models\Operation');
    }

    public function attribute() {
        return $this->belongsTo('App\Models\Attribute');
    }

}

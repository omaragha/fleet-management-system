<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WatchedVehicle extends Model
{

    protected $fillable = [
        'start_date',
        'end_date',
        'vehicle_id'
    ];

    public function vehicle() {
        return $this->belongsTo('App\Models\Vehicle');
    }

}

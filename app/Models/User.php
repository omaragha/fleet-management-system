<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Symfony\Component\HttpFoundation\Request;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function role() {
        return $this->belongsTo('App\Models\Role');
    }

    public function company() {
        return $this->belongsTo('App\Models\Company');
    }

    public function assignments() {
        return $this->hasMany('App\Models\Assignment');
    }

    public function checklists() {
        return $this->belongsToMany('App\Models\Checklist', 'user_checklist');
    }

    public function visits() {
        return $this->hasMany('App\Models\Visit');
    }

    public function issues() {
        return $this->hasMany('App\Models\Issue');
    }

    public function reports() {
        return $this->hasMany('App\Models\ProblemReport');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{

    protected $fillable = [
        'location',
        'start_date',
        'end_date',
        'vehicle_id',
        'user_id',
        'checklist_type_id',
        'actual_date',
        'checklist_id',
        'status'
    ];

    public function vehicle() {
        return $this->belongsTo('App\Models\Vehicle');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function justification() {
        return $this->belongsTo('App\Models\Justification');
    }

    public function checklistType() {
        return $this->belongsTo('App\Models\ChecklistType');
    }

    public function attributes() {
        return $this->belongsToMany('App\Models\Attribute', 'visit_attribute')->withPivot('id');
    }

    public function statuses() {
        return $this->hasMany('App\Models\Status');
    }

}

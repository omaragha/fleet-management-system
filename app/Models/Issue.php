<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{

    protected $fillable = [
        'name',
        'description',
        'user_id',
        'item_failure_id',
        'report_problem_id',
        'vehicle_id',
        'odometer',
        'end_date',
        'deadline',
        'image',
        'priority',
        'status'
    ];

    public function operator() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function item() {
        return $this->belongsTo('App\Models\ItemFailure', 'item_failure_id');
    }

    public function spareparts() {
        return $this->hasMany('App\Models\Sparepart');
    }

}

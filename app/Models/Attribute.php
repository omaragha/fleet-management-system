<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{

    protected $fillable = [
        'section_id',
        'status',
        'type',
        'name',
        'input_type_id',
        'operation_id',
        'validation_valid',
        'validation_invalid',
        'is_required'
    ];

    public function rules() {
        return $this->hasMany('App\Models\Rule');
    }

    public function section() {
        return $this->belongsTo('App\Models\Section');
    }

    public function operation() {
        return $this->belongsTo('App\Models\Operation');
    }

    public function inputType() {
        return $this->belongsTo('App\Models\InputType');
    }

    public function selectItems() {
        return $this->hasMany('App\Models\SelectItem');
    }

    public function visits() {
        return $this->belongsToMany('App\Models\Visit', 'visit_attribute')->withPivot('id');
    }



}

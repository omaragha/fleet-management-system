<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sparepart extends Model
{

    protected $fillable = [
        'name',
        'code',
        'description',
        'quantity',
        'count',
        'image',
        'sparepart_type_id',
        'issue_id',
        'visit_attribute_id',
        'vehicle_id'
    ];

    public function issue() {
        return $this->belongsTo('App\Models\Issue');
    }

    public function vehicle() {
        return $this->belongsTo('App\Models\Vehicle');
    }

    public function sparepartType() {
        return $this->belongsTo('App\Models\SparepartType');
    }

}

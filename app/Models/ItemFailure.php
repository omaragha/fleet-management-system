<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemFailure extends Model
{

    protected $fillable = [
        'status',
        'justification',
        'visit_attribute_id'
    ];

    public function issues() {
        return $this->hasMany('App\Models\Issue');
    }

}

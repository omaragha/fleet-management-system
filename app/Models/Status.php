<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

    protected $fillable = ['name','visit_id'];


    public function visit() {
        return $this->belongsTo("App\Models\Visit");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SparepartType extends Model
{

    protected $fillable = [
        'name',
        'description'
    ];

    public function spareparts() {
        return $this->hasMany('App\Models\Sparepart');
    }

    public function reports() {
        return $this->hasMany('App\Models\ProblemReport');
    }
}

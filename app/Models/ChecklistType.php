<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChecklistType extends Model
{

    protected $fillable = ['name'];

    public function checklists() {
        return $this->hasMany('App\Models\Checklist');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'location',
        'address',
        'package_id',
        'phone',
        'field',
        'email'
    ];

    public function users() {
        return $this->hasMany('App\Models\User');
    }

    public function vehicles() {
        return $this->hasMany('App\Models\Vehicle');
    }

    public function vehicleModels() {
        return $this->belongsToMany('App\Models\VehicleModel')->withPivot('id');
    }

    public function package() {
        return $this->belongsTo('App\Models\package');
    }

}

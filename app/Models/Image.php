<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    protected $fillable = [
        'real_name',
        'saved_name',
        'path',
        'visit_attribute_id'
    ];

    public function attribute() {
        return $this->belongsTo('App\Models\Attribute');
    }

}

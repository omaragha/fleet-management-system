<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    protected $fillable = [
        'name',
        'checklist_id'
    ];

    public function checklist() {
        return $this->belongsTo('App\Models\Checklist');
    }

    public function attributes() {
        return $this->hasMany('App\Models\Attribute');
    }

}

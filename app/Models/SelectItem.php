<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SelectItem extends Model
{

    protected $fillable = ['value', 'attribute_id'];

}

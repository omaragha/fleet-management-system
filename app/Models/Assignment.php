<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = [
        'start_date',
        'end_date',
        'start_hour',
        'end_hour',
        'comments',
        'cost',
        'vehicle_id',
        'user_id',
        'actual_end_date'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function vehicle() {
        return $this->belongsTo('App\Models\Vehicle');
    }

}

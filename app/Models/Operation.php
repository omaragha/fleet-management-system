<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{

    protected $fillable = [
        'operation'
    ];

    public function attributes() {
        return $this->hasMany('App\Models\Attribute');
    }

    public function rules() {
        return $this->hasMany('App\Models\Rule');
    }

}

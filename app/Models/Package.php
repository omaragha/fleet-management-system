<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'min_size',
        'max_size'
    ];

    public function companies() {
        return $this->hasMany('App\Models\Company');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputType extends Model
{
    
    protected $fillable = [
        'name'
    ];
    
    public function attributes() {
        return $this->hasMany('App\Models\Attribute');
    }

}

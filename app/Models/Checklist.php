<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{

    protected $fillable = ['name', 'checklist_type_id', 'status'];

    public function checklistType() {
        return $this->belongsTo('App\Models\ChecklistType');
    }

    public function users() {
        return $this->belongsToMany('App\Models\User', 'user_checklist');
    }

    public function sections() {
        return $this->hasMany('App\Models\Section');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramVisit extends Model
{

    protected $fillable = ['start_date', 'end_date', 'service_program_id'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'id',
        'name',
        'type',
        'company_vehicle_model_id',
        'company_id',
        'group_id',
        'serial_number',
        'license_plate',
        'year',
        'status',
        'color',
        'in_service_date',
        'in_service_odmeter',
        'estimated_service_life_in_month',
        'estimated_service_life_in_mile',
        'out_of_service_date',
        'out_of_service_odmeter',
        'width',
        'length',
        'passenger_volume',
        'empty_weight',
        'full_weight',
        'engine_summary',
        'engine_brand',
        'body_type',
        'aspiration',
        'block_type',
        'valves',
        'drive_type',
        'brake_system',
        'oil_capacity',
        'fuel_type',
        'fuel_tank_capacity',
    ];

    public function assignments() {
        return $this->hasMany('App\Models\Assignment');
    }

    public function watchedVehicles() {
        return $this->hasMany('App\Models\WatchedVehicle');
    }

    public function maintenances() {
        return $this->hasMany('App\Models\Maintenance');
    }

    public function visits() {
        return $this->hasMany('App\Models\Visit');
    }

    public function vehicleModel() {
        return $this->belongsTo('App\Models\VehicleModel');
    }

    public function companies() {
        return $this->belongsToMany('App\Models\Company');
    }

    public function equipements() {
        return $this->belongsToMany('App\Models\Equipement', 'vehicle_equipement');
    }

    public function companyVehicleModel() {
        return $this->belongsTo('App\Models\CompanyVehicleModel');
    }

    public function servicePrograms() {

        return $this->hasMany("App\Models\ServiceProgram");
    }

    public function reports() {
        return $this->hasMany('App\Models\ProblemReport');
    }

    public function company() {
        return $this->belongsTo('App\Models\Company');
    }

    public function issues() {
        return $this->hasMany('App\Models\Issue');
    }

    public function images() {
        return $this->hasMany('App\Models\Image');
    }
}

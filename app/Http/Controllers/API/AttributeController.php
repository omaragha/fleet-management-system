<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Attribute;
use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\API\RuleController;
use App\Http\Controllers\API\SelectItemController;
use Illuminate\Support\Facades\DB;

class AttributeController extends BaseController
{

    public function create(Request $request) {

        $request->validate([
            'name'                => 'required',
            'validationValid'     => 'required',
            'validationInvalid'   => 'required',
            'sectionId'           => 'required|exists:sections,id',
            'inputTypeId'         => 'required|exists:input_types,id',
            'sparepartTypeid'     => 'exists:sparepart_types,id',
            'isRequired'          => 'required'
        ]);

        try {

            $data = [
                'name'               => $request->name,
                'validation_valid'   => $request->validationValid,
                'validation_invalid' => $request->validationInvalid,
                'status'             => 0,
                'section_id'         => $request->sectionId,
                'input_type_id'      => $request->inputTypeId,
                'input_type_id'      => $request->inputTypeId,
                'is_required'        =>$request->isRequired
            ];

            if ($request->has('sparepartTypeid')) {
                $data['sparepart_type_id'] = $request->sparepartTypeId;
            }

            $attribute = Attribute::create($data);

            $attribute->refresh();

            return $this->sendResponse($attribute, 'Attribute added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $attributeId) {

        $request->validate([
            'name'                => 'required',
            'validationValid'     => 'required',
            'validationInvalid'   => 'required',
            'sparepartTypeId'     => 'exists:sparepart_types,id',
            'validationInvalid'   => 'required'
        ]);

        try {

            $attribute = Attribute::find($attributeId);

            if (!$attribute) {
                throw new Exception('Attribute NOT found!', 404);
            }

            $attribute->name = $request->name;
            $attribute->validation_valid   = $request->validationValid;
            $attribute->validation_invalid = $request->validationInvalid;

            if ($request->has('sparepartTypeId')) {
                $attribute->sparepart_type_id = $request->sparepartTypeId;
            }

            $attribute->is_required = $request->isRequired;

            $attribute->save();

            return $this->sendResponse([], 'Attribute updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function delete($attributeId) {

        try {

            $attribute = Attribute::find($attributeId);

            if (!$attribute) {
                throw new Exception('Attribute NOT found!', 404);
            }

            $attribute->delete();

            return $this->sendResponse([], 'Attribute deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }
}

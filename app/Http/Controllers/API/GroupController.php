<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends BaseController
{

    public function index() {

        try {
            $groups = Group::all(['id', 'name']);
            return $this->sendResponse($groups, 'Getting groups successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'name' => 'required'
        ]);

        try {
            Group::create($request->all());
            return $this->sendResponse([], 'Group created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($groupId) {

        $request = new Request(['groupId' => $groupId]);

        $request->validate([
            'groupId' => 'required|exists:groups,id'
        ]);

        try {
            $group = Group::find($groupId, ['id', 'name']);
            return $this->sendResponse($group, 'Getting group successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $groupId) {

        $request->merge(['groupId' => $groupId]);

        $request->validate([
            'groupId' => 'required|exists:groups,id',
            'name'    => 'required'
        ]);

        try {

            $group = Group::find($groupId);
            $group->name = $request->name;
            $group->save();

            return $this->sendResponse([], 'Group updated successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($groupId) {

        $request = new Request(['groupId' => $groupId]);

        $request->validate([
            'groupId' => 'required|exists:groups,id'
        ]);

        try {

            $group = Group::find($groupId);

            $group->delete();

            return $this->sendResponse([], 'Group deleted successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

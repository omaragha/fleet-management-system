<?php

namespace App\Http\Controllers\API\Checklists;

use App\Http\Controllers\API\BaseController;
use App\Models\ChecklistType;
use Illuminate\Http\Request;

class ChecklistTypeController extends BaseController
{

    public function index() {

        try {
            $checklistTypes = ChecklistType::all(['id', 'name']);
            return $this->sendResponse($checklistTypes, 'Getting checklist types successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error', 500);
        }

    }

    public function create(Request $request) {
        $request->validate([
            'types' => 'required|array'
        ]);

        try {

            foreach ($request->types as $key => $type) {
                ChecklistType::create([
                    'name' => $type
                ]);
            }

            return $this->sendResponse([], 'Checklist type added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }
    }

    public function update(Request $request, $checklistTypeId) {

        $request->validate([
            'name' => 'required'
        ]);

        try {

            $checklistType = ChecklistType::find($checklistTypeId);

            if (!$checklistType) {
                throw new Exception('Checklist NOT found!', 422);
            }

            $checklistType->name = $request->name;
            $checklistType->save();

            return $this->sendResponse([], 'Checklist Type updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 422) {
                return $this->sendError($th->getMessage(), $th->getCode());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

}

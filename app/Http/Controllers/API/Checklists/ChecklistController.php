<?php

namespace App\Http\Controllers\API\Checklists;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\API\CompanyVehicleModel;
use App\Models\Attribute;
use App\Models\Checklist;
use App\Models\ChecklistType;
use App\Models\Image;
use App\Models\Rule;
use App\Models\Section;
use App\Models\Vehicle;
use App\Models\Visit;
use App\SelectItem;
use App\Traits\ImageTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class ChecklistController extends BaseController
{

    use ImageTrait;

    public function index() {

        try {

            $checklists = Checklist::with('checklistType')
                ->withCount('sections')
                ->get()
                ->map(function ($item) {

                    $item->checklistType = $item->checklistType['name'];

                    return $item;


                });


            return $this->sendResponse($checklists);

        } catch (\Throwable $th) {

            $th->getMessage();

            return $this->sendError('error');

        }
    }

//    public function getNumberOfVehicles() {
//
//            $vehicle = db::table('vehicles')
//                ->select(DB::raw('count(*) as vehicleCounts'))
//                ->leftjoin('company_vehicle_model',
//                    'vehicles.company_vehicle_model_id',
//                    '=',
//                    'company_vehicle_model.id')
//                ->leftjoin('user_checklist',
//                    'company_vehicle_model.id',
//                    '=',
//                    'user_checklist.company_vehicle_model_id')
//                ->leftjoin('checklists','user_checklist.checklist_id','=','checklists.id')
//                ->get();
//
//        return $vehicle;
//    }


    public function getChecklistDetails($checklistId) {

        try {

            $data = [];

            $checklist = Checklist::where('id', $checklistId)
                ->with(
                    'sections.attributes.rules.operation',
                    'sections.attributes.inputType',
                    'sections.attributes.selectItems',
                    'checklistType')->get()->first();

            if (!$checklist) {
                throw new Exception('Checklist NOT found!', 404);
            }

            $data['checklistTypeId'] = $checklist->checklistType->id;
            $data['type'] = $checklist->checklistType->name;
            $data['id'] = $checklist->id;
            $data['name'] = $checklist->name;
            $data['status'] = $checklist->status;

            $storedSections = $checklist->sections;

            $sections = [];

            foreach ($storedSections as $key2 => $section) {

                $storedAttributes = $section->attributes;

                $attributes = [];

                foreach ($storedAttributes as $key3 => $attribute) {

                    $storedRules = $attribute->rules;

                    $rules = [];

                    $storedItems = $attribute->selectItems;

                    $items = [];

                    foreach ($storedRules as $key4 => $rule) {

                        $rules[] = [
                            'id'            => $rule->id,
                            'value'         => $rule->value,
                            'operationId'   => $rule->operation->id,
                        ];

                    }

                    foreach ($storedItems as $key4 => $item) {

                        $items[] = [
                            'id'    => $item->id,
                            'value' => $item->value
                        ];

                    }

                    $attributes[] = [
                        'id'                => $attribute->id,
                        'name'              => $attribute->name,
                        'inputTypeId'       => $attribute->inputType->id,
                        'isRequired'        => $attribute->is_required,
                        'inputTypeName'     => $attribute->inputType->name,
                        'validationValid'   => $attribute->validation_valid,
                        'validationInvalid' => $attribute->validation_invalid,
                        'sparepartTypeId'   => $attribute->sparepart_type_id,
                        'critical'          => $attribute->critical,
                        'rules'             => $rules,
                        'selectItems'       => $items
                    ];

                }

                $sections[] = [
                    'id'         => $section->id,
                    'name'       => $section->name,
                    'attributes' => $attributes
                ];
            }

            $data['sections'] = $sections;

            return $this->sendResponse($data, 'Getting checklists successfully.');

        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function create(Request $request) {

        $request->validate([
            'name'            => 'required',
            'checklistTypeId' => 'required|exists:checklist_types,id',
            'status'          => 'in:' . implode(',', ['private', 'public']),
            'companyModel'    => 'exists:company_vehicle_model,id'
        ]);

        try {

            $data = $request->all();

            $data['checklist_type_id'] = $request->checklistTypeId;

            unset($data['checklistTypeId']);

            $checklist = Checklist::create($data);

            $checklist->refresh();

            $data = [
                'id' => $checklist->id
            ];

            $request = new Request([
                'checklistId'     => $checklist->id,
                'checklistTypeId' => $checklist->checklist_type_id,
                'companyModel'    => $request->companyModel
            ]);

            $this->linkChecklistWithUser($request);

            return $this->sendResponse($data, 'Checklist added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error.', 500);
        }
    }

    public function update(Request $request, $checklistId) {

        $request->validate([
            'name' => 'required'
        ]);

        try {

            $checklist = Checklist::find($checklistId);

            if (!$checklist) {
                throw new Exception('Checklist NOT found!', 404);
            }

            $checklist->name = $request->name;
            $checklist->save();

            return $this->sendResponse([], 'Checklist updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getMessage() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function delete($checklistId) {

        try {

            $checklist = Checklist::find($checklistId);

            if (!$checklist) {
                throw new Exception('Checklist NOT found!', 404);
            }

            $checklist->delete();

            return $this->sendResponse([], 'Checklist deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function linkChecklistWithUser(Request $request) {

        try {

            DB::table('user_checklist')->insert([
                'user_id'                  => auth()->user()->id,
                'checklist_id'             => $request->checklistId,
                'checklist_type_id'        => $request->checklistTypeId,
                'company_vehicle_model_id' => $request->companyModel
            ]);

            return $this->sendResponse([], 'Checklist linked successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function createTemplate(Request $request) {

        $request->validate([
            'checklistId'     => 'required|exists:checklists,id',
            'name'            => 'required',
            'checklistTypeId' => 'required|exists:checklist_types,id',
            'status'          => 'in:' . implode(',', ['private', 'public']),
            'companyModel'    => 'exists:company_vehicle_model,id'
        ]);

        try {

            $result = $this->create($request);

            $newChecklistId = $result->getData()->data->id;

            if ($result->getStatusCode() != 200) {
                throw new Exception($result->getData()->message, $result->getStatusCode());
            }

            $data = $this->getChecklistDetails($request->checklistId);

            if ($data->getStatusCode() != 200) {
                throw new Exception();
            }

            foreach ($data->getData()->data->sections as $key => $section) {

                $newSection = Section::create([
                    'name'         => $section->name,
                    'checklist_id' => $result->getData()->data->id
                ]);

                foreach ($section->attributes as $key1 => $attribute) {

                    $newAttribute = Attribute::create([
                        'name'               => $attribute->name,
                        'section_id'         => $newSection->id,
                        'input_type_id'      => $attribute->inputTypeId,
                        'validation_valid'   => $attribute->validationValid,
                        'validation_invalid' => $attribute->validationInvalid,
                        'is_required'        => $attribute->isRequired,
                        'critical'           => $attribute->critical,
                        'sparepart_type_id'  => $attribute->sparepartTypeId,
                    ]);

                    foreach ($attribute->rules as $key2 => $rule) {

                        $newRule = Rule::create([
                            'value'        => $rule->value,
                            'operation_id' => $rule->operationId,
                            'attribute_id' => $newAttribute->id
                        ]);

                    }

                    foreach ($attribute->selectItems as $key2 => $item) {

                        $newItem = SelectItem::create([
                            'value'        => $item->value,
                            'attribute_id' => $newAttribute->id
                        ]);

                    }

                }

            }

            return $this->sendResponse(['id'=>$newChecklistId], 'Template checklist created successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }
    }

}

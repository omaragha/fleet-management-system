<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Models\Package;

class packageController extends BaseController
{
    public function index() {

        try {
            $packages = Package::all(['id', 'min_size', 'max_size','name']);
            return $this->sendResponse($packages, 'Getting packages successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }
    }

}
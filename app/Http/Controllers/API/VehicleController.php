<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Image;
use App\Models\Vehicle;
use App\Models\Visit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class VehicleController extends BaseController
{

    public function index()
    {
        try {
            $result = DB::table('vehicles')
                        ->join('company_vehicle_model', 'vehicles.company_vehicle_model_id', '=', 'company_vehicle_model.id')
                        ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                        ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
                        ->join('groups','vehicles.group_id','=','groups.id')
                        ->distinct()
                        ->get([
                            'vehicles.id',
                            'vehicles.name',
                            'company_vehicle_model.id as companyVehicleModel',
                            'vehicle_models.name as type',
                            'serial_number as serialNumber',
                            'license_plate as licensePlate',
                            'year',
                            'main_vehicle_models.id as make',
                            'company_vehicle_model.id as model',
                            'vehicles.status',
                            'groups.name as group',
                            // 'vehicles.group_id as group',
                            'vehicles.color',
                            'vehicles.body_type as bodyType',
                            'vehicles.in_service_date as inServiceDate',
                            'vehicles.in_service_odmeter as inServiceOdmeter',
                            'vehicles.estimated_service_life_in_month as estimatedServiceLifeInMonth',
                            'vehicles.estimated_service_life_in_mile as estimatedServiceLifeInMile',
                            'vehicles.out_of_service_date as outOfServiceDate',
                            'vehicles.out_of_service_odmeter as outOfServiceOdmeter',
                            'vehicles.width',
                            'vehicles.height',
                            'vehicles.length',
                            'vehicles.passenger_volume as passengerVolume',
                            'vehicles.empty_weight as emptyWeight',
                            'vehicles.full_weight as fullWeight',
                            'vehicles.engine_summary as engineSummary',
                            'vehicles.engine_brand as engineBrand',
                            'vehicles.aspiration',
                            'vehicles.block_type as blockType',
                            'vehicles.valves',
                            'vehicles.drive_type as driveType',
                            'vehicles.brake_system as brakeSystem',
                            'vehicles.oil_capacity as oilCapacity',
                            'vehicles.fuel_type as fuelType',
                            'vehicles.fuel_tank_capacity as fuelTankCapacity'
                        ]);

            $vehicles = [];

            foreach ($result as $key => $value) {

                $vehicle = (array) $value;

                $rst = DB::table('checklist_service_program')
                       ->where('vehicle_id', $value->id)
                       ->get([
                           'id',
                           'checklist_id as checklistId',
                           'user_id as operatorId',
                           'service_program_id as programId'
                       ]);

                $vehicle['preventiveMaintenance'] = $rst;

                $vehicles[] = $vehicle;

            }


            return $this->sendResponse($vehicles, 'Getting vehicles successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Serer Error!', 500);
        }
    }

    public function create(Request $request)
    {


        $request->validate([
            'serialNumber'                        => 'unique:vehicles,serial_number',
            'name'                                => 'required',
            'model'                               => 'required|exists:company_vehicle_model,id',
            'preventiveMaintenance'               => 'required|array',
            'preventiveMaintenance.*.operatorId'  => 'required|exists:users,id',
            'preventiveMaintenance.*.checklistId' => 'required|exists:checklists,id',
            'preventiveMaintenance.*.programId'   => 'required|exists:service_programs,id',
            'group'                               => 'required|exists:groups,id',
        ]);

        try {

            $myRequest = new Request();

            foreach ($request->all() as $key => $value) {
                if ($key == 'preventiveMaintenance') continue;
                $myRequest->merge([$key => $value]);
            }

            $myRequest = $this->fixRequest($myRequest);

            $vehicle = Vehicle::create($myRequest->all() + ['company_id' => auth()->user()->company->id]);

            foreach ($request->preventiveMaintenance as $key => $value) {
                DB::table('checklist_service_program')
                    ->insert([
                        'checklist_id'       => $value['checklistId'],
                        'service_program_id' => $value['programId'],
                        'user_id'            => $value['operatorId'],
                        'vehicle_id'         => $vehicle->id,
                    ]);
            }

            return $this->sendResponse([], 'Vehicle created successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function edit($vehicleId) {

        try {

            $vehicle = DB::table('vehicles')
                    ->join('company_vehicle_model', 'vehicles.company_vehicle_model_id', '=', 'company_vehicle_model.id')
                    ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                    ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
                    ->join('groups','vehicles.group_id','=','groups.id')
                    ->where('vehicles.id', $vehicleId)
                    ->distinct()
                    ->get([
                        'vehicles.id',
                        'vehicles.name',
                        'vehicles.image',
                        'vehicle_models.name as type',
                        'serial_number as serialNumber',
                        'license_plate as licensePlate',
                        'year',
                        'main_vehicle_models.id as make',
                        'company_vehicle_model.id as model',
                        'main_vehicle_models.name as makeName',
                        'vehicle_models.name as modelName',
                        'vehicles.group_id as group',
                        // 'vehicles.service_program_id as preventiveMaintenance',
                        'vehicles.status',
                        'vehicles.group_id as group',
                        // 'vehicles.user_id as operator',
                        'vehicles.color',
                        'vehicles.body_type as bodyType',
                        'vehicles.in_service_date as inServiceDate',
                        'vehicles.in_service_odmeter as inServiceOdmeter',
                        'vehicles.estimated_service_life_in_month as estimatedServiceLifeInMonth',
                        'vehicles.estimated_service_life_in_mile as estimatedServiceLifeInMile',
                        'vehicles.out_of_service_date as outOfServiceDate',
                        'vehicles.out_of_service_odmeter as outOfServiceOdmeter',
                        'vehicles.width',
                        'vehicles.height',
                        'vehicles.length',
                        'vehicles.passenger_volume as passengerVolume',
                        'vehicles.empty_weight as emptyWeight',
                        'vehicles.full_weight as fullWeight',
                        'vehicles.engine_summary as engineSummary',
                        'vehicles.engine_brand as engineBrand',
                        'vehicles.aspiration',
                        'vehicles.block_type as blockType',
                        'vehicles.valves',
                        'vehicles.drive_type as driveType',
                        'vehicles.brake_system as brakeSystem',
                        'vehicles.oil_capacity as oilCapacity',
                        'vehicles.fuel_type as fuelType',
                        'vehicles.fuel_tank_capacity as fuelTankCapacity'
                ])
                ->first();


            if (!$vehicle) {
                throw new Exception('Vehicle NOT found!', 404);
            }

            $preventiveMaintenance = DB::table('checklist_service_program')
                                     ->where('vehicle_id', $vehicle->id)
                                     ->get([
                                         'id',
                                         'checklist_id as checklistId',
                                         'user_id as operatorId',
                                         'service_program_id as programId'
                                     ]);

            $vehicle = (array) $vehicle;

            $vehicle['preventiveMaintenance'] = $preventiveMaintenance;

            return $this->sendResponse($vehicle, 'Getting vehicle info successfully.');

        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError($th->getMessage(), 500);
            }
        }

    }

    public function update(Request $request, $id)
    {

        $request->merge(['vehicleId' => $id]);

        // Validation request fields.
        $request->validate([
            'vehicleId'                           => 'required|exists:vehicles,id',
            'serialNumber'                        => 'unique:vehicles,serial_number,' . $id,
            'name'                                => 'required',
            'model'                               => 'required|exists:company_vehicle_model,id',
            // 'operator'                            => 'required|exists:users,id',
            'group'                               => 'required|exists:groups,id',
            'preventiveMaintenance'               => 'required|array',
            'preventiveMaintenance.*.checklistId' => 'required|exists:checklists,id',
            'preventiveMaintenance.*.operatorId'  => 'required|exists:users,id',
            'preventiveMaintenance.*.programId'   => 'required|exists:service_programs,id',
        ]);


        try {

            $vehicle = Vehicle::find($id);

            if (!$vehicle) {
                throw new Exception('Vehicle NOT found!', 404);
            }

            $myRequest = new Request();

            foreach ($request->all() as $key => $value) {
                if ($key == 'preventiveMaintenance') continue;
                $myRequest->merge([$key => $value]);
            }

            $myRequest = $this->fixRequest($myRequest);

            $vehicle->update($request->all());

            DB::table('checklist_service_program')
                ->where('vehicle_id', $id)
                ->delete();

            foreach ($request->preventiveMaintenance as $key => $value) {
                DB::table('checklist_service_program')
                    ->insert([
                        'checklist_id'       => $value['checklistId'],
                        'service_program_id' => $value['programId'],
                        'user_id'            => $value['operatorId'],
                        'vehicle_id'         => $vehicle->id,
                    ]);
            }

            return $this->sendResponse([], 'Vehicle updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function delete($id)
    {
        try {

            DB::table('checklist_service_program')
                ->where('vehicle_id', $id)
                ->delete();

            $vehicle = Vehicle::find($id);

            if (!$vehicle) {
                throw new Exception('Vehicle NOT found!', 404);
            }

            $vehicle->delete();
            return $this->sendResponse([], 'Vehicle deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function addImages(Request $request, $vehicleId) {

        $request->merge(['vehicleId' => $vehicleId]);

        $request->validate([
            'images'    => 'required|array',
            'vehicleId' => 'required|exists:vehicles,id'
        ]);

        try {

            // $result = $this->deleteImages($vehicleId);

            // if ($result->getStatusCode() != 200) {
            //     throw new Exception('Internal Server Error!', 500);
            // }

            $images = [];


            foreach ($request->images as $key => $image) {

                $img = Image::create([
                    'path' => $image
                ]);

                $img->vehicle_id = $vehicleId;

                $images[] = $img;

                $img->save();
            }

            return $this->sendResponse($images, 'Images added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function deleteImages($vehicleId) {

        try {

            Image::where('vehicle_id', $vehicleId)->delete();

            return $this->sendResponse([], 'Images deleted successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function visits(Request $request , $id) {

        try {

            $vehicle = Vehicle::with(["visits","visits.checklistType"])
            ->where('id',$id)
            ->get()
            ->first();

            foreach ($vehicle->visits as $key => $value) {
                $vehicle->visits[$key]['visitType'] = $value['checklistType']['name'];
            }


            return $this->sendResponse($vehicle->visits,"Getting data successfully");

        } catch (\Throwable $th) {
            //throw $th;

            echo $th->getMessage();

            return $this->sendError('error');

        }
    }

    public function updateLocation(Request $request) {

        $request->validate([
            'lat'       => 'required|numeric',
            'lon'       => 'required|numeric',
        ]);

        try {

            if (auth()->user()->role->name != 'driver') {
                throw new Exception('Unauthorized', 401);
            }

            $vehicleId = $this->getCurrentVehicleForDriver(auth()->user()->id)->getData()->data[0]->id;;

            $vehicle = Vehicle::find($vehicleId);

            $vehicle->lat = $request->lat;
            $vehicle->lon = $request->lon;

            $vehicle->save();

            return $this->sendResponse([], 'Location updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 401) {
                return $this->sendError($th->getMessage(), $th->getCode());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }


    public function startCMVisit(Request $request,$vehicleId) {

        try {


            if(!$vehicleId) {

                return $this->sendError('my friend you should send a vehicle id');

            }

            $request->validate([
                "operatorId"  =>"required",
                "checklistId" =>"required",
                "endDate"     =>"required",
                "startDate"   =>'required'
            ]);




            $visit =  Visit::create([
                'vehicle_id'        => $vehicleId,
                'user_id'           => $request->operatorId,
                'checklist_type_id' => 2,
                'end_date'        => $request->endDate,
                'start_date'          => $request->startDate,
                'checklist_id'      => $request->checklistId,
            ]);


            $serviceProgram = new ServiceProgramController();

            $serviceProgram->initChecklist($visit,$visit->checklist_id);

            return $this->sendResponse([],'visit created successfully');

        } catch (\Throwable $th) {

            return $this->sendError([],'error');

        }

    }


    public function getLocation() {

        try {

            if (auth()->user()->role->name != 'driver') {
                throw new Exception('Unauthorized', 401);
            }

            $vehicleId = $this->getCurrentVehicleForDriver(auth()->user()->id)->getData()->data[0]->id;

            $data = Vehicle::find($vehicleId, ['id', 'lat', 'lon']);

            return $this->sendResponse($data, 'Getting location of vehicle successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
            if ($th->getCode() == 401) {
                return $this->sendError($th->getMessage(), $th->getCode());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function getItemFailuresByVehicle($vehicleId) {

        $request = new Request(['vehicleId' => $vehicleId]);

        $request->validate([
            'vehicleId' => 'required|exists:vehicles,id'
        ]);

        try {

            $data = DB::table('vehicles')
                    ->join('visits', 'vehicles.id', '=', 'visits.vehicle_id')
                    ->join('visit_attribute', 'visits.id', '=', 'visit_attribute.id')
                    ->join('item_failures', 'visit_attribute.id', '=', 'item_failures.visit_attribute_id')
                    ->join('attributes', 'visit_attribute.attribute_id', '=', 'attributes.id')
                    ->join('sparepart_types', 'attributes.sparepart_type_id', '=', 'sparepart_types.id')
                    ->where('vehicles.id', $vehicleId)
                    ->get([
                        'item_failures.id',
                        'vehicles.serial_number',
                        'sparepart_types.name',
                        'item_failures.justification',
                        'item_failures.status',
                        'item_failures.created_at'
                    ])
                    ->map(function ($item) {
                        return [
                            'id'            => $item->id,
                            'serialNumber'  => $item->serial_number,
                            'sparepartType' => $item->name,
                            'justification' => $item->justification,
                            'status'        => $item->status,
                            'date'          => date('Y-m-d', strtotime($item->created_at))
                        ];
                    });

            return $this->sendResponse($data, 'Getting failures by vehicle successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function issues($vehicleId) {

        try {
            
            $vehicle = Vehicle::with('issues')
            ->where('id',$vehicleId)
            ->get()
            ->first();


            return $this->sendResponse($vehicle->issues,'success');
            // ->
        }catch (\Throwable $th) {

            return $this->sendError($th->getMessage(),500);
            
        }
    }

    public function images($vehicleId) {

        try {
            
            $vehicle = Vehicle::with('images')
            ->where('id',$vehicleId)
            ->get()
            ->first();


            return $this->sendResponse($vehicle->images,'success');
            // ->
        }catch (\Throwable $th) {

            return $this->sendError($th->getMessage(),500);
            
        }
    }


    public function setProfileImage($vehicleId) {

        $defaultImage = request()->image;
        try {
            
            $vehicle = Vehicle::find($vehicleId);
            $image = Image::find($defaultImage);

            $vehicle->image = $image->path;
            
            $vehicle->save();

            return $this->sendResponse($image,'success');
            // ->
        }catch (\Throwable $th) {

            return $this->sendError($th->getMessage(),500);
            
        }
    }

    public function getVehicleVisitsDetails($vehicleId) {
        

        $data['nextVisit'] = $this->getNextVisit($vehicleId)['start_date'];
        $data['lastVisit'] = $this->getLastVisit($vehicleId)['end_date'];
        $data['numberOfVisits'] = $this->getNumberOfVehicleVisits($vehicleId);
        $data['getOverDueVisits'] = $this->getOverDueVisits($vehicleId);

        return $this->sendResponse($data,'success');
        
    }

    public function getNextVisit($vehicleId){

        try {
            
            $currentDate = date('Y-m-d');

            $vehicle = Vehicle::with(['visits'=>function($query) use ($currentDate){

                $query
                ->whereNull('actual_date')
                ->where('end_date','>=',$currentDate)
                ->orderBy('end_date')
                ->limit(1);
            }])
            ->where('id',$vehicleId)
            ->get()
            ->first();

            return $vehicle->visits[0];

        }catch (\Throwable $th) {

            return $this->sendError($th->getMessage(),500);
            
        }

    }

    
    public function getLastVisit($vehicleId){

        try {
            
            $vehicle = Vehicle::with(['visits'=>function($query){

                $query
                ->whereNotNull('actual_date')
                ->orderByDesc('actual_date')
                ->limit(1);
            }])
            ->where('id',$vehicleId)
            ->get()
            ->first();

            return $vehicle->visits[0];

        }catch (\Throwable $th) {

            return $this->sendError($th->getMessage(),500);
            
        }

    }

    public function getNumberOfVehicleVisits($vehicleId) {


        try {
            
            $count = Vehicle::withCount('visits')
            ->where('id',$vehicleId)
            ->get()
            ->first();

            return $count->visits_count;

        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage(),500);
        }

    }

    public function getOverDueVisits($vehicleId) {

        try {
            
            $currentDate = date('Y-m-d');

            $count = Vehicle::withCount(['visits'=>function($query) use($currentDate){

                $query->whereNull('visits.actual_date')
                ->orWhere('actual_date','>=','end_date');

            }])
            ->where('id',$vehicleId)
            ->get()
            ->first();

            return $count;

        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage(),500);
        }

    }

    public function changeVehicleStatus($vehicleId) {



        try {

            $vehicle = Vehicle::find($vehicleId);

            if(isset(request()->status)) {
                $vehicle->status = request()->status;
            }
    
            $vehicle->save();

            return $this->sendResponse([],'success');
            

        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage(),500);
        }




    }


    


}

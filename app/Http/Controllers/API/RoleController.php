<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Privilege;
use App\Models\Role;
use Exception;
use Illuminate\Http\Request;

class RoleController extends BaseController
{

    public function index() {

        try {

            // Get all roles in the system.
            $roles = Role::all(['id', 'name', 'description']);

            return $this->sendResponse($roles, 'Getting roles successfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function create(Request $request) {

        // Validation request fields.
        $request->validate([
            'name'        => 'required',
            'description' => 'required'
        ]);

        try {
            Role::create($request->all());
            return $this->sendResponse([], 'Role created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function edit($roleId) {

        try {

            $role = Role::find($roleId, ['id', 'name', 'description']);

            if (!$role) {
                throw new Exception('Role NOT found!', 404);
            }
            
            return $this->sendResponse($role, 'Getting role info successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function update(Request $request, $roleId) {

        // Validation request fields.
        $request->validate([
            'name'        => 'required',
            'description' => 'required'
        ]);

        try {

            $role = Role::find($roleId);

            if (!$role) {
                throw new Exception('Role NOT found!', 404);
            }

            $role->name = $request->name;
            $role->description = $request->description;

            $role->save();

            return $this->sendResponse([], 'Role updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function delete ($roleId) {

        try {

            $role = Role::find($roleId);

            if (!$role) {
                throw new Exception('Role NOT found!', 404);
                
            }

            $role->delete();

            return $this->sendResponse([], 'Role deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function assignPrivilegeToRole(Request $request) {

        $request->validate([
            'roleId'    => 'required',
            'companyId' => 'required'
        ]);

        try {
            $role = Role::find($request->roleId);

            if (!$role) {
                throw new Exception('Role NOT found!', 404);
            }

            $privilege = Privilege::find($request->privilegeId);

            if (!$privilege) {
                throw new Exception('Privilege NOT found!', 404);
            }

            $role->privileges()->attach($request->roleId);

            return $this->sendResponse([], 'Privilege assigned to role successfully');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }

    }

}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;

use App\Models\Justification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JustificationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        try {
            
            $justifications = Justification::all();
            return $this->sendResponse($justifications,"Getting justifications correctly");

        } catch (\Throwable $th) {

            return $this->sendError($th->getMessage());

        }
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Justification  $justification
     * @return \Illuminate\Http\Response
     */
    public function show(Justification $justification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Justification  $justification
     * @return \Illuminate\Http\Response
     */
    public function edit(Justification $justification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Justification  $justification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Justification $justification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Justification  $justification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Justification $justification)
    {
        //
    }
}

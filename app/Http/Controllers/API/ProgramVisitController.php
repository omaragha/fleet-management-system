<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\ProgramVisit;
use Illuminate\Http\Request;

class ProgramVisitController extends BaseController
{

    public function index($serviceProgramId = null) {

        try {

            if ($serviceProgramId) {
                $programVisits = ProgramVisit::where('service_program_id', $serviceProgramId)->get(['id', 'start_date as startDate', 'end_date as endDate']);
            }

            return $this->sendResponse($programVisits, 'Getting program visits succesfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'serviceProgramId'   => 'required|exists:service_programs,id',
            'visits'             => 'required|array',
            'visits.*.startDate' => 'required|date',
            'visits.*.endDate'   => 'required|date',
        ]);

        try {

            foreach ($request->visits as $key => $visit) {
                ProgramVisit::create([
                    'start_date'         => $visit['startDate'],
                    'end_date'           => $visit['endDate'],
                    'service_program_id' => $request->serviceProgramId
                ]);
            }

            return $this->sendResponse([], 'Program visit created succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

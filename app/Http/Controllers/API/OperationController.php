<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Operation;
use Exception;
use Illuminate\Http\Request;

class OperationController extends BaseController
{

    public function index() {

        try {

            $operations = Operation::all(['id', 'operation']);

            return $this->sendResponse($operations, 'Getting operations successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'operation' => 'required'
        ]);

        try {

            $operation = Operation::create([
                'operation' => $request->operation
            ]);

            return $this->sendResponse([], 'Operation created successfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($operationId) {

        try {

            $operation = Operation::find($operationId, ['id', 'operation']);

            if (!$operation) {
                throw new Exception('Operation NOT found!', 404);
            }

            return $this->sendResponse($operation, 'Getting operation info successfully.');
        } catch (\Throwable $th) {
            
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function update(Request $request, $operationId) {

        $request->validate([
            'operation' => 'required'
        ]);

        try {

            $operation = Operation::find($operationId);

            if (!$operation) {
                throw new Exception('Operation NOT found!', 404);
            }

            $operation->operation = $request->operation;

            $operation->save();

            return $this->sendResponse([], 'Operation updated successfully.');
        } catch (\Throwable $th) {
            
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function delete($operationId) {
        try {


            $operation = Operation::find($operationId);

            if (!$operation) {
                throw new Exception('Operation NOT found!', 404);
            }

            $operation->delete();

            return $this->sendResponse([], 'Operation deleted successfully.');
        } catch (\Throwable $th) {
            
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\SparepartType;
use Illuminate\Http\Request;

class SparepartTypeController extends BaseController
{

    public function index()
    {

        try {
            $sparepartTypes = SparepartType::all();
            return $this->sendResponse($sparepartTypes, 'Getting sparepart types successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'description' => 'description'
        ]);

        try {
            $sparepartType = SparepartType::create($request->all());
            return $this->sendResponse(['id' => $sparepartType->id], 'Sparepart type created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($sparepartTypeId)
    {

        $request = new Request(['sparepartTypeId' => $sparepartTypeId]);

        $request->validate([
            'sparepartTypeId' => 'required|exists:sparepart_types,id'
        ]);

        try {
            $sparepartType = SparepartType::find($sparepartTypeId, ['id', 'name']);
            return $this->sendResponse($sparepartType, 'Getting sparepart type successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $sparepartTypeId)
    {

        $request->merge(['sparepartTypeId' => $sparepartTypeId]);

        $request->validate([
            'sparepartTypeId' => 'required|exists:sparepart_types,id'
        ]);

        try {
            $sparepartType = SparepartType::find($sparepartTypeId);

            $sparepartType->name = $request->name;
            $sparepartType->description = $request->description;

            $sparepartType->save();

            return $this->sendResponse([], 'Sparepart type updated successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($sparepartTypeId) {

        $request = new Request(['sparepartTypeId' => $sparepartTypeId]);

        $request->validate([
            'sparepartTypeId' => 'required|exists:sparepart_types,id'
        ]);

        try {

            SparepartType::find($sparepartTypeId)->delete();

            return $this->sendResponse([], 'Sparepart type deleted successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

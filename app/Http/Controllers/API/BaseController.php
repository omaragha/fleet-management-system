<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Assignment;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class BaseController extends Controller
{

    public function sendResponse($result = [], $message = '')
    {
        $response = [
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }

    public function sendError($message = '', $status = 404)
    {
        $response = [
            'data'    => [],
            'message' => $message,
        ];
        return response()->json($response, $status);
    }

    public function checkEmailIfExists($email) {
        $user = User::where('email', $email)->get()->first();
        return $user;
    }

    public function deleteUserTokens($userId) {
        $user = User::findOrFail($userId);
        $user->tokens()->each(function ($token, $key) {
            $token->delete();
        });
    }

    public function getPreviliges($userId) {

        $roleId = auth()->user()->role->id;

        $privileges = Role::find($roleId)->privilegs;

        return $privileges;
    }

    public function deleteFile($path)
    {
        if (file_exists($path)) {
            unlink($path);
        }
    }

    public function fixRequest(Request $request) {

        $fixedRequest = new Request();

        foreach ($request->all() as $key => $value) {

            $fixedKey = '';

            if ($key == 'model') {
                $fixedKey = 'company_vehicle_model_id';
            } else if ($key == 'service_program') {
                $fixedKey = 'service_program_id';
            } else if ($key == 'operator') {
                $fixedKey = 'user_id';
            } else if ($key == 'group') {
                $fixedKey = 'group_id';
            } else if ($key == 'make') {
                $fixedKey = 'main_vehicle_model_id';
            } else {
                for($i = 0; $i < strlen($key); $i++) {

                    if (ctype_upper($key[$i])) {
                        $fixedKey .= '_';
                    }

                    $fixedKey .= strtolower($key[$i]);

                }
            }

            $fixedRequest->merge([$fixedKey => $value]);

        }

        return $fixedRequest;
    }

    public function getCurrentVehicleForDriver($driverId) {

        $request = new Request(['driverId' => $driverId]);

        $request->validate([
            'driverId' => 'required|exists:users,id'
        ]);

        try {

            $currentTime = time();

            $date = date('Y-m-d', $currentTime);

            $vehicle = Assignment::where('user_id', $driverId)
                                ->whereDate('start_date', '<=', $date)
                                ->whereDate('end_date', '>=', $date)
                                ->where(function ($query) use ($currentTime) {
                                    $query->where('start_hour', null)
                                          ->orWhere([
                                              ['start_hour', '<=', date('Y-m-d H:i:s', $currentTime)],
                                              ['end_hour', '>=', date('Y-m-d H:i:s', $currentTime)]
                                          ]);
                                })
                                ->with('user', 'vehicle')
                                ->get()
                                ->map(function ($item) {
                                    return [
                                        'id'          => $item->vehicle->id,
                                        'vehicelName' => $item->vehicle->name
                                    ];
                                });
            return $this->sendResponse($vehicle, 'Getting current vehicle for driver successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function getCurrentDriverForVehicle($vehicleId) {

        $request = new Request(['vehicleId' => $vehicleId]);

        $request->validate([
            'vehicleId' => 'required|exists:vehicles,id'
        ]);

        try {

            $currentTime = time();

            // return dd(date('Y-m-d H:i:s', $currentTime));

            $date = date('Y-m-d', $currentTime);

            $driver = Assignment::where('vehicle_id', $vehicleId)
                                ->whereDate('start_date', '<=', $date)
                                ->whereDate('end_date', '>=', $date)
                                ->where(function ($query) use (&$currentTime) {
                                    $query->where('start_hour', null)
                                          ->orWhere([
                                              ['start_hour', '<=', date('Y-m-d H:i:s', $currentTime)],
                                              ['end_hour', '>=', date('Y-m-d H:i:s', $currentTime)]
                                          ]);
                                })
                                ->with('user', 'vehicle')
                                ->get()
                                ->map(function ($item) {
                                    return [
                                        'id'       => $item->user->id,
                                        'operator' => $item->user->first_name . ' ' . $item->user->last_name
                                    ];
                                });

            return $this->sendResponse($driver, 'Getting current driver for vehicle successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

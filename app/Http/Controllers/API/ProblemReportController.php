<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\ProblemReport;
use Illuminate\Http\Request;

class ProblemReportController extends BaseController
{

    public function index() {

        try {

            $reports = ProblemReport::where(
                'status', '!=', 'checked')
                ->with('user', 'sparepartType', 'vehicle')->get()->map(function ($item) {

                return [
                    'id'              => $item->id,
                    'name'            => $item->name,
                    'image'           => $item->image,
                    'description'      => $item->description,
                    'operatorId'      => $item->user->id,
                    'operator'        => $item->user->first_name . ' ' . $item->user->last_name,
                    'sparepartTypeId' => $item->sparepartType->id,
                    'sparepartType'   => $item->sparepartType->name,
                    'vehicleId'       => $item->vehicle->id,
                    'vehicleName'     => $item->vehicle->name,
                ];

            });

            return $this->sendResponse($reports, 'Getting problem reports successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        if (auth()->user()->role->id != 2) {
            return $this->sendError('Unauthorized', 401);
        }

        $request->validate([
            'sparepartTypeId' => 'required|exists:sparepart_types,id',
            'name'            => 'required',
        ]);

        try {

            $report = ProblemReport::create([
                'user_id'           => auth()->user()->id,
                'vehicle_id'        => $this->getCurrentVehicleForDriver(auth()->user()->id)->getData()->data[0]->id,
                'sparepart_type_id' => $request->sparepartTypeId,
                'name'              => $request->name,
                'image'             => $request->image,
                'description'       => $request->description
            ]);

            return $this->sendResponse(['id' => $report->id], 'Problem report created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($problemId) {

        
        try {

            $problem = ProblemReport::with('user', 'sparepartType', 'vehicle')
            ->where('id',$problemId)
            ->get()
            ->map(function($item) {

                return [
                    'id'              => $item->id,
                    'name'            => $item->name,
                    'image'           => $item->image,
                    'description'      => $item->description,
                    'operatorId'      => $item->user->id,
                    'operator'        => $item->user->first_name . ' ' . $item->user->last_name,
                    'sparepartTypeId' => $item->sparepartType->id,
                    'sparepartType'   => $item->sparepartType->name,
                    'vehicleId'       => $item->vehicle->id,
                    'vehicleName'     => $item->vehicle->name,
                    'reportedDate '   => $item->create_at
                ];

            });
            
            
            return $this->sendResponse($problem[0],'successs');

            
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }


    }

    public function delete($problemId) {



        try {

            $problem = ProblemReport::find($problemId);

            $problem->delete();

            return $this->sendResponse([],'successs');

            
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }


        
    }

    public function changeStatus($problemId) {

        try {

            $problem  = ProblemReport::find($problemId);

            if (isset(request()->status)) {
                $problem->status = request()->status;
            }

            $problem->save();
            
            return $this->sendResponse([],'successs');

        }  catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }
    
    }

}

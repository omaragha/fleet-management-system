<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Privilege;
use Illuminate\Http\Request;
use Exception;

class PrivilegeController extends BaseController
{

    public function index() {

        try {
            $privileges = Privilege::all(['name', 'description']);
            return $this->sendResponse($privileges, 'Getting privileges successfully');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function create(Request $request) {

        // Validation request fields.
        $request->validate([
            'name'        => 'required',
            'description' => 'required'
        ]);

        try {
            $privilege = new Privilege();
            $privilege->name = $request->name;
            $privilege->description = $request->description;
            $privilege->save();

            return $this->sendResponse([], 'Privilege created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function edit($privilegeId) {

        try {

            $privilege = Privilege::find($privilegeId, ['id', 'name', 'description']);

            if (!$privilege) {
                throw new Exception('Privilege NOT found!', 404);
            }

            return $this->sendResponse($privilege, 'Getting privilege info successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function update(Request $request, $privilegeId) {

        // Validation request fields.
        $request->validate([
            'name'        => 'required',
            'description' => 'required'
        ]);

        try {
            $privilege = Privilege::find($privilegeId);
            
            if (!$privilege) {
                throw new Exception('Privilege NOT found!', 404);
            }

            $privilege->name = $request->name;
            $privilege->description = $request->description;

            $privilege->save();

            return $this->sendResponse([], 'Privilege updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function delete ($privilegeId) {

        try {
            $privilege = Privilege::find($privilegeId);

            if (!$privilege) {
                throw new Exception('Privilege NOT found!', 404);
            }
            
            $privilege->delete();

            return $this->sendResponse([], 'Privilege deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }

    }

}

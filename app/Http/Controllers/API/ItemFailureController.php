<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\ItemFailure;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemFailureController extends BaseController
{

    public function index($vehicleId = null) {

        $condictions = [];

        if ($vehicleId) {

            $request = new Request(['vehicleId' => $vehicleId]);

            $request->validate([
                'vehicleId' => 'required|exists:vehicles,id'
            ]);

            $condictions[] = [
                'vehicles.id', $vehicleId
            ];
        }

        $condictions[] = [
            'item_failures.status','pending'
        ];
        

        $itemFailures = $this->getItemFailures($condictions);

        if($itemFailures!=null) {

            return $this->sendResponse($itemFailures, 'Getting item failures succesfully.');

        } else {
            
            return $this->sendError('Internal server error','error');
        
        }

    }

    public function view($itemFailureId) {

        if(!$itemFailureId) {
            return $this->sendError('Please send an item failure id');
        }

       $image_sub_query = DB::table('images');

       $data =  DB::table('item_failures')
        ->join('visit_attribute','item_failures.visit_attribute_id','=','visit_attribute.id')
        ->join('visits','visit_attribute.visit_id','=','visits.id')
        ->join('users','visits.user_id','=','users.id')
        ->join('checklist_types','visits.checklist_type_id','=','checklist_types.id')
        ->join('vehicles','visits.vehicle_id','=','vehicles.id')
        ->join('groups','vehicles.group_id','=','groups.id')
        ->join('company_vehicle_model','vehicles.company_vehicle_model_id','company_vehicle_model.id')
        ->join('vehicle_models','company_vehicle_model.vehicle_model_id','=','vehicle_models.id')
        ->join('main_vehicle_models','vehicle_models.main_vehicle_model_id','main_vehicle_models.id')
        ->join('attributes','visit_attribute.attribute_id','=','attributes.id')
        ->join('sections','attributes.section_id','=','sections.id')
        ->where('item_failures.id',$itemFailureId)
        ->get([
            'item_failures.id',
            'users.id as operator',
            'vehicles.id as vehicleId',
            'groups.name as vehicleGroup',
            'vehicles.name as vehicleName',
            'vehicle_models.name as model',
            'main_vehicle_models.name as make',
            'checklist_types.name as visitType',
            'sections.name as equipmentName',
            'attributes.name as checkName',
            'visit_attribute.id as visitAttributeId',
            'visit_attribute.value as checkValue',
            'visit_attribute.value as checkJustification',
            'visits.start_date as startDate',
            'visits.actual_date as actualDate',
            'users.first_name as firstName',
            'users.last_name as lastName'
        ])
        ->first();

        $imgs = Image::where('visit_attribute_id', $data->visitAttributeId)->get(['path']);

        //coming soon hh

        // $spareparts = Spareparts::where('visit_attribute_id',$data->visitAttributeId)
        // ->get();

        $images = [];

        foreach ($imgs as $key2 => $image) {
            $images[] = $image->path;
        }

        $data->images = $images;

        //$data->spareparts = $spareparts;

        $data->spareparts = [];


        return $this->sendResponse($data);

    }

    public function itemFailuresReport() {
        
        $condictions = [];

        $itemFailures =  $this->getItemFailures($condictions);

        if($itemFailures!=null) 
            return $this->sendResponse($itemFailures);
        else 
            return $this->sendError('error','error');
            
    }

    public function getItemFailures($condictions = []) 
    {

            $itemFailures = DB::table('item_failures')
                ->join('visit_attribute', 'item_failures.visit_attribute_id', '=', 'visit_attribute.id')
                ->join('visits', 'visit_attribute.visit_id', '=', 'visits.id')
                ->join('vehicles', 'visits.vehicle_id', '=', 'vehicles.id')
                ->join('users', 'visits.user_id', '=', 'users.id')
                ->join('attributes', 'visit_attribute.attribute_id', '=', 'attributes.id')
                ->join('sections', 'attributes.section_id', '=', 'sections.id')
                ->where($condictions)
                ->select(
                    DB::raw("
                        item_failures.id,
                        FORMAT(visits.actual_date, 'yyyy-MM-dd') as actualDate,
                        vehicles.name as vehicleName,
                        attributes.name as attributeName,
                        sections.name as sectionName,
                        CONCAT(users.first_name, ' ', users.last_name) as operator,
                        item_failures.status,
                        item_failures.justification
                    ")
                )->get();

            return $itemFailures;
        

    }

    public function update(Request $request, $itemId) {

        $request->merge(['itemId' => $itemId]);

        $request->validate([
            'itemId' => 'required|exists:item_failures,id',
            'status' => 'required|in:' . implode(',', ['closed', 'acknowledged', 'pending'])
        ]);

        try {

            $item = ItemFailure::find($itemId);

            $item->justification = $request->justification;
            $item->status = $request->status;
            
            $item->save();

            return $this->sendResponse([], 'Item updated succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

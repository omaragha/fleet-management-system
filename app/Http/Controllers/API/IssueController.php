<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Issue;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\FuncCall;

class IssueController extends BaseController
{

    public function index() {

        $conditions = [];

        if (auth()->user()->role->id != 1) {
            $conditions[] = [
                'users.id', auth()->user()->id
            ];
        }

        try {

            $issues = DB::table('issues')
                      ->join('users', 'issues.user_id', '=', 'users.id')
                      ->leftjoin('vehicles','issues.vehicle_id','=','vehicles.id')
                      ->leftJoin('item_failures', 'issues.item_failure_id', '=', 'item_failures.id')
                      ->leftJoin('spareparts', 'issues.id', '=', 'spareparts.issue_id')
                      ->where($conditions)
                      ->whereIn('issues.status', ['pending', 'running'])
                      ->select(
                          DB::raw("
                            issues.id,
                            issues.name,
                            issues.priority,
                            issues.deadline,
                            issues.description,
                            vehicles.id as vehicleId,
                            vehicles.company_vehicle_model_id as vehicleModel,
                            CONCAT(users.first_name, ' ', users.last_name) as operator,
                            issues.status,
                            item_failures.justification
                          ")
                      )
                      ->get();


            return $this->sendResponse($issues, 'Getting issues successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'operator'      => 'required|exists:users,id',
            'vehicleId'     => 'exists:vehicles,id',
            'itemFailureId' => 'exists:item_failures,id',
        ]);

        try {

            $issue = Issue::create([
                'name'            => $request->name,
                'description'     => $request->description,
                'item_failure_id' => $request->itemFailureId,
                'vehicle_id'      => $request->vehicleId,
                'user_id'         => $request->operator,
                'end_date'        => $request->enddate,
                'deadline'        => $request->deadline,
                'image'           => $request->image,
                'odometer'        => $request->odometer,
                'report_problem_id'       => $request->problemId
            ]);

            return $this->sendResponse(['id' => $issue->id], 'Issue created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($issueId) {

        $request = new Request(['issueId' => $issueId]);

        $request->validate([
            'issueId' => 'required|exists:issues,id'
        ]);

        try {

            $issue = Issue::with(['operator','spareparts','item'])
            ->where('id',$issueId)
            ->get()
            ->map(function($item) {
                return [
                    'id'           => $item->id,
                    'itemFailureId' => $item->item_failure_id,
                    'reportProblemId' =>$item->report_problem_id,
                    'createdAt'    => date('Y-m-d', strtotime($item->created_at)),
                    'deadline'     => $item->deadline,
                    'operator'     => $item->operator->first_name . ' ' . $item->operator->last_name,
                    'description'  => $item->description,
                    'status'       => $item->status,
                    'vehicleId'    => $item->vehicle_id,
                    'spareparts'   => $item->spareparts
                ];
            })
            ->first();

            return $this->sendResponse($issue,'success');
        } catch (\Throwable $th) {
            return $this->sendResponse($th, 'Getting Issue details successfully.');
        }

    }

    public function update(Request $request , $issueId) {

        if(!$issueId) {
            return $this->sendError("send and id my friend",'error');
        }

        try {

            $issue = Issue::find($issueId);

            $issue->name = $request->name;
            $issue->description = $request->description;
            $issue->user_id = $request->operator;
            $issue->end_date = $request->enddate;
            $issue->deadline = $request->deadline;
            $issue->image = $request->image;
            $issue->priority = $request->priority;
            $issue->odometer = $request->odometer;


            $issue->save();

            return $this->sendResponse([],'success');

        }  catch (\Throwable $th) {
            return $this->sendResponse($th, 'Getting Issue details successfully.');
        }





    }


    public function solve($issueId) {

        try {
           
            $issue = Issue::find($issueId);

            $issue->status = 'solved';
    
            $issue->save();

            return $this->sendResponse([],'success');

        } catch (\Throwable $th) {
           
            return $this->sendError($th->getMessage(),500);

        }



    }


    public function close($issueId) {

        try {
           
            $issue = Issue::find($issueId);

            $issue->status = 'closed';
    
            $issue->save();
            
            return $this->sendResponse([],'success');

        } catch (\Throwable $th) {
           
            return $this->sendError($th->getMessage(),500);

        }



    }


    public function delete($issueId) {
        try {
            $issue = Issue::find($issueId);

            $issue->delete();

            return $this->sendResponse([],'success');
        }
        catch (\Throwable $th) {
            return $this->sendResponse($th->getMessage(), 'successfully.');
        }
    }

    public function issuesReport() {

        try {
            
            $issues = Issue::all();

            return $this->sendResponse($issues,'success');

        } catch (\Throwable $th) {
            
            return $this->sendError($th->getMessage(),500);

        }
    }

}

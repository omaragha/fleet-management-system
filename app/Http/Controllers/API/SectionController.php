<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Checklist;
use App\Models\Section;
use Exception;
use Illuminate\Http\Request;

class SectionController extends BaseController
{

    public function create(Request $request) {

        $request->validate([
            'name'        => 'required',
            'checklistId' => 'required|exists:checklists,id'
        ]);

        try {

            $section = Section::create([
                        'name'         => $request->name,
                        'checklist_id' => $request->checklistId
                    ]);

            $data = [
                'id' => $section->id
            ];

            return $this->sendResponse($data, 'Section added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $sectionId) {

        $request->validate([
            'name'        => 'required',
            'checklistId' => 'exists:checklists,id'
        ]);

        try {

            $section = Section::find($sectionId);

            if (!$section) {
                throw new Exception('Section NOT found!', 404);
            }

            $section->name = $request->name;

            if ($request->has('checklistId')) {
                $section->checklist_id = $request->checklistId;
            }

            $section->save();

            return $this->sendResponse([], 'Section updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function delete($sectionId) {

        try {

            $section = Section::find($sectionId);

            if (!$section) {
                throw new Exception('Section NOT found!', 404);
            }

            $section->delete();

            return $this->sendResponse([], 'Section deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

}

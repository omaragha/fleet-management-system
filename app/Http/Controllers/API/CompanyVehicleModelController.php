<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Company;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyVehicleModelController extends BaseController
{

    public function index() {

        try {

            $data = Company::where('id', auth()->user()->company->id)->with('vehicleModels')->get(['id', 'name', 'location'])->first();

            if (!$data) {
                throw new Exception('Company NOT found!', 404);
            }

            $companyVehicleModels = [];

            foreach ($data->vehicleModels as $key => $companyVehicleModel) {

                $companyVehicleModels[] = [
                    'id'   => $companyVehicleModel['pivot']['id'],
                    'name' => $companyVehicleModel['name'],
                ];

            }

            return $this->sendResponse($companyVehicleModels, 'Getting company vehicle models successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function create(Request $request) {

        $request->validate([
            'vehicleModelId'   => 'required|array'
            // ,
            // 'vehicleModelId.*' => 'exists:vehicle_models,id'
        ]);

        try {

            $company = Company::find(auth()->user()->company->id);

            $company->vehicleModels()->syncWithoutDetaching($request->vehicleModelId);

            return $this->sendResponse([], 'Company model added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($companyVehicleModelId) {

        try {

            $company = Company::find(auth()->user()->company->id);

            $company->vehicleModels()->wherePivot('id', $companyVehicleModelId)->detach();

            return $this->sendResponse([], 'Company vehicle model deleted successfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }


    public function checklistsSuggestion($companyVehicleModelId) {

        $request = new Request(['companyVehicleModelId' => $companyVehicleModelId]);

        $request->validate([
            'companyVehicleModelId' => 'required|exists:company_vehicle_model,id'
        ]);

        try {

            $mainModelId = DB::table('company_vehicle_model')
                        ->where('id', $companyVehicleModelId)
                        ->get(['vehicle_model_id'])
                        ->first()
                        ->vehicle_model_id;

            $subModelChecklists = DB::table('company_vehicle_model')
                                ->join('user_checklist', 'company_vehicle_model.id', '=', 'user_checklist.company_vehicle_model_id')
                                ->join('checklists', 'user_checklist.checklist_id', '=', 'checklists.id')
                                ->where('company_vehicle_model.id', $companyVehicleModelId)
                                ->get()
                                ->map(function ($item) {
                                    return [
                                        'id'   => $item->id,
                                        'name' => $item->name,
                                        'type' => 'subModel'
                                    ];
                                })
                                ->toArray();

            $mainModelChecklists = DB::table('company_vehicle_model')
                                ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                                ->join('user_checklist', 'company_vehicle_model.id', '=', 'user_checklist.company_vehicle_model_id')
                                ->join('checklists', 'user_checklist.checklist_id', '=', 'checklists.id')
                                ->where('vehicle_models.id', $mainModelId)
                                ->get()
                                ->map(function ($item) {
                                    return [
                                        'id'   =>$item->id,
                                        'name' => $item->name,
                                        'type' => 'mainModel'
                                    ];
                                })
                                ->toArray();

            $globalChecklists = DB::table('user_checklist')
                                ->join('checklists', 'user_checklist.checklist_id', '=', 'checklists.id')
                                ->whereNull('company_vehicle_model_id')
                                ->get()
                                ->map(function ($item) {
                                    return [
                                        'id'   => $item->id,
                                        'name' => $item->name,
                                        'type' => 'public'
                                    ];
                                })
                                ->toArray();

            $data = [];

            $result = [];

            $result = array_merge($result, $subModelChecklists);
            $result = array_merge($result, $mainModelChecklists);
            $result = array_merge($result, $globalChecklists);

            $visited = [];

            $data = array_filter($result, function ($item) use (&$visited) {
                if (!isset($visited[$item['id']])) {
                    $visited[$item['id']] = true;
                    return $item;
                }
            });

            $data = array_values($data);

            return $this->sendResponse($data, 'Getting suggestion checklists succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Models\WatchedVehicle;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\API\Validator;

class WatcehdvehiclesController extends BaseController
{

    public function index()
    {
        try {
        $watched_Vehicle = DB::table('watched_vehicles')->get();
        return $this->sendResponse($watched_Vehicle, 'Getting Watched Vehicle successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function create(Request $request)
    {   $data = [ 'data' => $request->all() ];
        //var_dump($data);
        for($i=0;$i<sizeof($request->vehicle);$i++){
            //var_dump($request->vehicle[$i]['startDate']);
            //var_dump($request->vehicle[$i]['endDate']);
            //var_dump($request->vehicle[$i]['vehicleId']);
            //$vehicles = $request->vehicle[$i];
            var_dump(sizeof($request->vehicle));
            /*$request->validate([
                $request->vehicle[$i]['startDate'] => 'required',
                $request->vehicle[$i]['endDate']   => 'required',
                $request->vehicle[$i]['vehicleId'] => 'required|exists:vehicles,id'           
            ]);*/    
            //try {
                WatchedVehicle::create([
                    'start_date' => $request->vehicle[$i]['startDate'],
                    'end_date'   => $request->vehicle[$i]['endDate'],
                    'vehicle_id' => $request->vehicle[$i]['vehicleId']
                ]);
                return $this->sendResponse([], 'Watched Vehicle created successfully.');
            //} catch (\Throwable $th) {
            //    return $this->sendError('Internal server error.', 500);
            //}
        }
    }

    public function update(Request $request, $id)
    {
        // Validation request fields.
        $request->validate([
            'startDate' => 'required',
            'endate'    => 'required', 
            'vehicleId' => 'required'
        ]);

        try {
        $watched_Vehicle = Watched_Vehicle::find($id);
        if (!$watched_Vehicle) {
            throw new Exception('Watched Vehicle NOT found!', 404);
        }
    
        $watched_Vehicle->start_date = $request->startDate;
        $watched_Vehicle->end_date = $request->endDate;
        $watched_Vehicle->vehicle_id = $request->vehicleId;

        $watched_Vehicle->save();

        return $this->sendResponse([], 'Watched Vehicle updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function delete($id)
    {
        try {
        $watched_Vehicle = WatchedVehicle::find($id);
    
        if (!$watched_Vehicle) {
            throw new Exception('Watched Vehicle NOT found!', 404);
        }
    
        $watched_Vehicle->delete();
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }
}

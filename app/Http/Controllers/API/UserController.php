<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use App\Models\Visit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends BaseController
{

    public function index()
    {

        try {

            if (isset(request()->type)) {

                $roleId = '';

                switch (request()->type) {
                    case 'admin':
                        $roleId = 1;
                        break;
                    case 'supervisor':
                        $roleId = 2;
                        break;
 
                    case 'technical':
                        $roleId = 3;
                        break;
                    case 'driver':
                        $roleId = 4;
                        break;
                    default:
                        # code...
                        break;
                }

                $users = User::where('role_id', $roleId)
                         ->select(
                             DB::raw("
                                id,
                                first_name as firstName,
                                last_name as lastName,
                                email,
                                start_date as startDate,
                                end_date as endDate,
                                address,
                                license_number as licenseNumber,
                                license_class as licenseClass,
                                end_of_license as endOfLicense,
                                CONCAT(users.first_name, ' ', users.last_name) as name
                             ")
                         )->get();

            } else {
                // check role.
                $users = User::select(
                    DB::raw("
                       id,
                       first_name as firstName,
                       last_name as lastName,
                       email,
                       start_date as startDate,
                       end_date as endDate,
                       address,
                       license_number as licenseNumber,
                       license_class as licenseClass,
                       end_of_license as endOfLicense,
                       CONCAT(users.first_name, ' ', users.last_name) as name
                    ")
                )->get();
            }

            return $this->sendResponse($users, 'Getting users successfully');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function create(Request $request)
    {

        // Validation request fields.
        $request->validate([
            'roleId'     => 'required',
            'firstName'  => 'required',
            'lastName'   => 'required',
            'email'      => 'required|email',
            'password'   => 'required|min:6'
        ]);

        try {

            if ($this->checkEmailIfExists($request->email)) {
                throw new Exception('E-mail already exists.', 422);
            }

            if (!Role::find($request->roleId)) {
                throw new Exception('Role NOT found!', 404);
            }


            // if (!Company::find($request->companyId)) {
            //     throw new Exception('Company NOT found!', 404);
            // }

            $user = new User();
            $user->role_id = $request->roleId;
            $user->company_id = auth()->user()->company->id;
            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->age = $request->age;
            $user->address = $request->address;
            $user->phone_number = $request->phoneNumber;
            $user->start_date = $request->startDate;
            $user->end_date = $request->endDate;
            $user->address = $request->address;
            $user->license_number = $request->licenseNumber;
            $user->license_class = $request->licenseClass;
            $user->save();

            return $this->sendResponse([], 'User created successfully.');
        } catch (\Throwable $th) {

            echo $th->getMessage();

            if ($th->getCode() == 422) {
                return $this->sendError($th->getMessage(), $th->getCode());
            } else if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function edit($userId) {

        try {

            $user = User::find($userId, [
                'id',
                 'first_name as firstName', 
                 'last_name as lastName', 
                 'email', 
                 'start_date as startDate', 
                 'end_date as endDate', 'address', 
                 'license_number as licenseNumber',
                 'license_class as licenseClass', 
                 'end_of_license as endOfLicense',
                 'role_id as roleId',
                 'age',
                 'phone_number as phoneNumber'
                  ]);

            if (!$user) {
                throw new Exception('User NOT found!', 404);
            }

            return $this->sendResponse($user, 'Getting users info successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function update(Request $request, $userId)
    {

        // Validation request fields.
        $request->validate([
            'firstName'  => 'required',
            'lastName'   => 'required',
            'email'      => 'required|email',
            'password'   => 'min:6'
        ]);

        try {

            $user = User::find($userId);

            if (!$user) {
                throw new Exception('User NOT found!', 404);
            }

            $passwordChanged = Hash::check($request->password, $user->password);

            // Check email if exists or not.
            if ($this->checkEmailIfExists($request->email) && strtolower($request->email) != strtolower($user->email)) {
                throw new Exception('E-mail already exists.', 422);
            }

            // Check if credential changed or not.
            if (($user->email != $request->email) || (!$passwordChanged)) {
                $this->deleteUserTokens($userId);
            }

            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->email = $request->email;

            if (!$passwordChanged) {
                $user->password = bcrypt($request->password);
            }

            $user->age = $request->age;
            $user->address = $request->address;
            $user->phone_number = $request->phoneNumber;

            $user->start_date = $request->startDate;
            $user->end_date = $request->endDate;
            $user->address = $request->address;
            $user->license_number = $request->licenseNumber;
            $user->license_class = $request->licenseClass;
            $user->end_of_license = $request->endOfLicense;

            $user->save();

            return $this->sendResponse([], 'User updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 422) {
                return $this->sendError($th->getMessage(), $th->getCode());
            } else if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function delete($userId)
    {

        try {
            $user = User::find($userId);

            if (!$user) {
                throw new Exception('User NOT found!', 404);
            }

            $user->delete();
            return $this->sendResponse([], 'User deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function getTechnicals() {

        try {
            $technicals = User::where('company_id', auth()->user()->company->id)->get(['id', 'fist_name as firstName', 'last_name as lastName']);
            return $this->sendResponse($technicals, 'Getting technicals successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function getAssignments(){
        $userId = auth()->user()->id;
        $assignments = DB::table('assignments')
                                ->join('users', 'assignments.user_id', '=', 'users.id')
                                ->join('vehicles','assignments.vehicle_id','=','vehicles.id')
                                ->where('assignments.user_id', $userId)
                                ->get([
                                    'assignments.start_date',
                                    'assignments.end_date',
                                    'assignments.cost',
                                    'vehicles.name'
                                ]);
        return $assignments;
    }

    public function getTasksByUser($userId) {

        $request = new Request(['userId' => $userId]);

        $request->validate([
            'userId' => 'required|exists:users,id'
        ]);

        try {

            $currentDate = date('Y-m-d H:i:s');

            $finishedTasks = [];

            $delayedTasks = [];

            $notFinishedTasks = [];

            $tasks = Visit::where('user_id', $userId)->get()->map(function ($item) {
                return [
                    'id'            => $item->id,
                    'startDate'     => date('Y-m-d', strtotime($item->start_date)),
                    'endDate'       => date('Y-m-d', strtotime($item->end_date)),
                    'actualEndDate' => $item->actual_date
                ];
            });

            foreach ($tasks as $key => $task) {

                if (is_null($task['actualEndDate'])) {
                    if ($currentDate <= $task['endDate']) {
                        $notFinishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                } else {
                    if ($task['actualEndDate'] >= $task['startDate'] && $task['actualEndDate'] <= $task['endDate']) {
                        $finishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                }

            }

            $data = [
                'finishedTasks'    => $finishedTasks,
                'delayedTasks'     => $delayedTasks,
                'notFinishedTasks' => $notFinishedTasks
            ];

            return $this->sendResponse($data, 'Getting tasks successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function getAssignmentsByUser($userId) {

        $request = new Request(['userId' => $userId]);

        $request->validate([
            'userId' => 'required|exists:users,id'
        ]);

        try {

            $currentDate = date('Y-m-d H:i:s');

            $finishedTasks = [];

            $delayedTasks = [];

            $notFinishedTasks = [];

            $tasks = Assignment::where('user_id', $userId)->get()->map(function ($item) {
                return [
                    'id'            => $item->id,
                    'startDate'     => date('Y-m-d', strtotime($item->start_date)),
                    'endDate'       => date('Y-m-d', strtotime($item->end_date)),
                    'startHour'     => date('H:i:s', strtotime($item->start_hour)),
                    'endHour'       => date('H:i:s', strtotime($item->end_hour)),
                    'actualEndDate' => $item->actual_end_date
                ];
            });

            foreach ($tasks as $key => $task) {

                if (is_null($task['actualEndDate'])) {
                    if ($currentDate <= $task['endDate']) {
                        $notFinishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                } else {
                    if ($task['actualEndDate'] >= $task['startDate'] && $task['actualEndDate'] <= $task['endDate']) {
                        $finishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                }

            }

            $data = [
                'finishedTasks'    => $finishedTasks,
                'delayedTasks'     => $delayedTasks,
                'notFinishedTasks' => $notFinishedTasks
            ];

            return $this->sendResponse($data, 'Getting assignments successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }


}

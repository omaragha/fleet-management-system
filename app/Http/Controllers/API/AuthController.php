<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;
use stdClass;

class AuthController extends BaseController
{

    public function register(Request $request)
    {

        // Check role of user.
        if ($request->user()->role->id != 1) {
            return $this->sendError('Unauthorized', 401);
        }

        // Validation input fields.
        $request->validate([
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6',
            'roleId'    => 'required|exists:roles,id',
            'companyId' => 'required|exists:companies,id'
        ]);

        try {

            User::create([
                'email'      => $request->email,
                'password'   => bcrypt($request->password),
                'role_id'    => $request->roleId,
                'company_id' => $request->companyId
            ]);

            return $this->sendResponse([], 'User added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }
    }

    public function login(Request $request)
    {

        // Validation input fields.
        $request->validate([
            'email'    => 'required|email',
            'password' => 'required'
        ]);

        $http = new \GuzzleHttp\Client();

        try {

            $http = new \GuzzleHttp\Client([
                'base_uri' => config('services.guzzle.base_url'),
            ]);

            // Post request to passport package.

            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->email,
                    'password' => $request->password,
                ],
            ]);
            $response = json_decode($response->getBody()->getContents());

            $data = new stdClass;

            $data->token = $response->access_token;

            $data->message = 'Logged in successfully.';

            $data->role = User::where('email', $request->email)->with('role')->get()->first()->role->name;

            return $this->sendResponse($data);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            return  $e->getMessage();
            if ($e->getCode() == 500) {
                return $this->sendError('Something went wrong on the server', $e->getCode());
            } else {
                return $this->sendError('Your credentials are incorrect. Please try again', $e->getCode());
            }
        }
    }


    public function logout()
    {

        $tokenId = (new Parser())->parse(request()->bearerToken())->getClaim('jti');

        $token = auth()->user()->tokens()->find($tokenId);

        $token->delete();

        return $this->sendResponse([], 'Logged out successfully');
    }
}

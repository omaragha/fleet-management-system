<?php

namespace App\Http\Controllers\API;

use App\Enums\ServiceProgramType;
use App\Http\Controllers\API\BaseController;
use App\Models\ServiceProgram;
use App\Models\Visit;
use App\Models\Checklist;
use App\Models\Status;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceProgramController extends BaseController
{

    public function index() {

        try {

            $data = [];

            $programVisit = new ProgramVisitController();

            $servicePrograms = ServiceProgram::where('company_id', auth()->user()->company->id)->get();

            foreach ($servicePrograms as $key => $serviceProgram) {

                $programVisits = $programVisit->index($serviceProgram->id);

                if ($programVisits->getStatusCode() != 200) {
                    throw new Exception($programVisits->getData()->message, $programVisits->getStatusCode());
                }

                $data[] = [
                    'id'             => $serviceProgram->id,
                    'name'           => $serviceProgram->name,
                    'description'    => $serviceProgram->description,
                    'type'           => $serviceProgram->type,
                    'numberOfVisits' => count($programVisits->getData()->data),
                    'visits'         => $programVisits->getData()->data
                ];

            }

            return $this->sendResponse($data, 'Getting service programs succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'name'               => 'required',
            'description'        => 'required',
            'programType'        => 'required|in:' . implode(',', ServiceProgramType::toArray()),
            'visits'             => 'array',
            'visits.*.startDate' => 'required',
            'visits.*.endDate'   => 'required',
        ]);

        try {

            $serviceProgram = ServiceProgram::create([
                'name'        => $request->name,
                'description' => $request->description,
                'type'        => $request->programType,
                'company_id'  => auth()->user()->company->id
            ]);

            $request->merge([
                'serviceProgramId' => $serviceProgram->id
            ]);

            $result = (new ProgramVisitController)->create($request);

            if ($result->getStatusCode() != 200) {
                throw new Exception($result->getData()->message, $result->getStatusCode());
            }

            return $this->sendResponse(['id' => $serviceProgram->id], 'Program service created succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($serviceProgramId) {

        $request = new Request(['serviceProgramId' => $serviceProgramId]);

        $request->validate([
            'serviceProgramId' => 'required|exists:service_programs,id'
        ]);

        try {

            $serviceProgram = ServiceProgram::find($serviceProgramId);

            $programVisits = (new ProgramVisitController)->index($serviceProgramId);

            if ($programVisits->getStatusCode() != 200) {
                throw new Exception($programVisits->getData(), $programVisits->getStatusCode());
            }

            $data = [
                'id'             => $serviceProgramId,
                'name'           => $serviceProgram->name,
                'description'    => $serviceProgram->description,
                'numberOfVisits' => count($programVisits->getData()->data),
                'visits'         => $programVisits->getData()->data
            ];

            return $this->sendResponse($data, 'Getting service program successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $serviceProgramId) {

        $request->merge(['serviceProgramId' => $serviceProgramId]);

        $request->validate([
            'serviceProgramId'   => 'required|exists:service_programs,id',
            'visits'             => 'required|array',
            'visits.*.startDate' => 'required|date',
            'visits.*.endDate'   => 'required|date',
        ]);

        try {

            $serviceProgram = ServiceProgram::find($serviceProgramId);

            $serviceProgram->programVisits()->delete();

            $serviceProgram->name = $request->name;
            $serviceProgram->description = $request->description;

            $serviceProgram->save();

            $result = (new ProgramVisitController)->create($request);

            if ($result->getStatusCode() != 200) {
                throw new Exception($result->getData()->message, $result->getStatusCode());
            }

            return $this->sendResponse([], 'Service program updated successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($serviceProgramId) {

        $request = new Request([
            'serviceProgramId' => $serviceProgramId
        ]);

        $request->validate([
            'serviceProgramId' => 'required|exists:service_programs,id'
        ]);

        try {

            $serviceProgram = ServiceProgram::find($serviceProgramId);

            $serviceProgram->delete();

            return $this->sendResponse([], 'Service program deleted successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function createVisits($serviceProgramId) {

        $request = new Request(['serviceProgramId' => $serviceProgramId]);

        $request->validate([
            'serviceProgramId' => 'required|exists:service_programs,id'
        ]);

        try {

            $serviceProgram = ServiceProgram::where('id', $serviceProgramId)->with('programVisits')->get()->first();

            $programVisits = $serviceProgram->programVisits;

            $data = DB::table('checklist_service_program')
                        ->join('checklists', 'checklist_service_program.checklist_id', '=', 'checklists.id')
                        ->join('checklist_types', 'checklists.checklist_type_id', '=', 'checklist_types.id')
                        ->where('service_program_id', $serviceProgramId)
                        ->get([
                            'checklist_service_program.id',
                            'checklist_service_program.vehicle_id',
                            'checklist_service_program.user_id',
                            'checklist_service_program.checklist_id',
                            'checklist_types.id as checklist_type_id'
                        ]);

            foreach ($data as $key => $value) {

                foreach ($programVisits as $key1 => $programVisit) {

                   $visit =  Visit::create([
                        'vehicle_id'        => $value->vehicle_id,
                        'user_id'           => $value->user_id,
                        'checklist_type_id' => $value->checklist_type_id,
                        'start_date'        => $programVisit->start_date,
                        'end_date'          => $programVisit->end_date,
                        'checklist_id'      => $value->checklist_id,
                        'status'            => 'pending'
                    ]);

                    Status::create([
                        'name'       =>'pending',
                        'visit_id'   => $visit->id
                    ]);
                    
                    $this->initChecklist($visit , $value->checklist_id);
                }

            }

            return $this->sendResponse([], 'Visits created succesfully.');
        } catch (\Throwable $th) {




            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function initChecklist($visit , $checklistId) {


        $data = Checklist::where('id', $checklistId)->with(["sections","sections.attributes"])
        ->get()
        ->first();

        foreach ($data->sections as $key => $section)
        {
        
            foreach ($section->attributes as $key1 => $attribute) {

                $visit->attributes()->syncWithoutDetaching($attribute->id, [
                    'justification' => '',
                    'value'         => '',
                    'images'        => [],
                    'spareparts'    => []
                ]);
            }
        }

        $visit->initialized = 1;

        $visit->save();

        $checklist = Checklist::find($checklistId);

        $checklist->used = 1;

        $checklist->save();

    }

}

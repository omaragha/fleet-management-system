<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\InputType;
use Exception;
use Illuminate\Http\Request;

class InputTypeController extends BaseController
{

    public function index() {

        try {
            
            $inputs = InputType::all(['id', 'name']);

            return $this->sendResponse($inputs, 'Getting inputs successfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'name' => 'required'
        ]);

        try {
            
            InputType::create([
                'name' => $request->name
            ]);

            return $this->sendResponse([], 'Input created successfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($inputId) {

        try {

            $input = InputType::find($inputId, ['id', 'name']);

            if (!$input) {
                throw new Exception('Input NOT found!', 404);
            }

            return $this->sendResponse($input, 'Getting input successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function update(Request $request, $inputId) {

        $request->validate([
            'name' => 'required'
        ]);

        try {

            $input = InputType::find($inputId);

            if (!$input) {
                throw new Exception('Input NOT found!', 404);
            }

            $input->name = $request->name;

            $input->save();

            return $this->sendResponse([], 'Input updated successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function delete($inputId) {

        try {

            $input = InputType::find($inputId);

            if (!$input) {
                throw new Exception('Input NOT found!', 404);
            }

            $input->delete();

            return $this->sendResponse([], 'Input deleted successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }
    
}

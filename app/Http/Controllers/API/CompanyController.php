<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\User;
use App\Models\Visit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends BaseController
{

    public function index() {

        try {
            $companies = Company::all(['id', 'name','location','phone','email','status']);
            return $this->sendResponse($companies, 'Getting companies successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }


    public function create(Request $request) {

        // Validation request fields.
        $request->validate([
            'company.name'       => 'required',
            'company.location'   => 'required'  ,
            'company.package_id' => 'required|exists:packages,id',
            'company.phone'      => 'required',
            'company.email'      => 'required|email',
            'company.field'      => 'required',
            'company.status'     => 'in:' . implode(',', ['pending','active','deactive']),
            'user.firstName'     => 'required',
            'user.lastName'      => 'required',
            'user.email'         => 'required|email|unique:users,email',
            'user.password'      => 'required|min:6'
        ]);

        try {

            $companyId = Company::create($request->company)->id;

            $userRequest = new Request($request->user);

            $userRequest->merge(['roleId' => 1]);

            $user = (new UserController())->create($userRequest);

            return $this->sendResponse([], 'Company created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function activate($companyId) {

        try {

            $company = Company::find($companyId);

            if (!$company) {
                throw new Exception('Company NOT found!', 404);
            }

            $company->status = "active";

            return $this->sendResponse($company, 'Activate company info successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function deactivate($companyId) {

        try {

            $company = Company::find($companyId);

            if (!$company) {
                throw new Exception('Company NOT found!', 404);
            }

            $company->status = "deactive";

            return $this->sendResponse($company, 'Activate company info successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function edit($companyId) {

        try {

            $company = Company::find($companyId, ['id', 'name', 'location']);

            if (!$company) {
                throw new Exception('Company NOT found!', 404);
            }

            return $this->sendResponse($company, 'Getting company info successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function update(Request $request, $companyId) {

        // Validation request fields.
        $request->validate([
            'name'     => 'required',
            'location' => 'required'
        ]);

        try {

            $company = Company::find($companyId);

            if (!$company) {
                throw new Exception('Company NOT found!', 404);
            }

            $company->name = $request->name;
            $company->location = $request->location;

            $company->save();

            return $this->sendResponse([], 'Company updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function delete($companyId) {

        try {
            $company = Company::find($companyId);

            if (!$company) {
                throw new Exception('Company NOT found!', 404);
            }

            $company->delete();
            return $this->sendResponse([], 'Company deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            }
            echo $th->getMessage();
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function getTasksByCompany($companyId) {

        $request = new Request(['companyId' => $companyId]);

        $request->validate([
            'companyId' => 'required|exists:companies,id'
        ]);

        try {

            $currentDate = date('Y-m-d H:i:s');

            $finishedTasks = [];

            $delayedTasks = [];

            $notFinishedTasks = [];

            $tasks = Visit::with(['user', 'user.company' => function ($query) use ($companyId) {
                return $query->where('id', $companyId);
            }])->get()->map(function ($item) {
                return [
                    'id'            => $item->id,
                    'startDate'     => date('Y-m-d', strtotime($item->start_date)),
                    'endDate'       => date('Y-m-d', strtotime($item->end_date)),
                    'actualEndDate' => $item->actual_date
                ];
            });

            foreach ($tasks as $key => $task) {

                if (is_null($task['actualEndDate'])) {
                    if ($currentDate <= $task['endDate']) {
                        $notFinishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                } else {
                    if ($task['actualEndDate'] >= $task['startDate'] && $task['actualEndDate'] <= $task['endDate']) {
                        $finishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                }

            }

            $data = [
                'finishedTasks'    => $finishedTasks,
                'delayedTasks'     => $delayedTasks,
                'notFinishedTasks' => $notFinishedTasks
            ];

            return $this->sendResponse($data, 'Getting tasks successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function getAssignmentsByCompany($companyId) {

        $request = new Request(['companyId' => $companyId]);

        $request->validate([
            'companyId' => 'required|exists:companies,id'
        ]);

        try {

            $currentDate = date('Y-m-d H:i:s');

            $finishedTasks = [];

            $delayedTasks = [];

            $notFinishedTasks = [];

            $tasks = Assignment::with(['user', 'user.company' => function ($query) use ($companyId) {
                return $query->where('id', $companyId);
            }])->get()->map(function ($item) {
                return [
                    'id'            => $item->id,
                    'startDate'     => date('Y-m-d', strtotime($item->start_date)),
                    'endDate'       => date('Y-m-d', strtotime($item->end_date)),
                    'startHour'     => date('H:i:s', strtotime($item->start_hour)),
                    'endHour'       => date('H:i:s', strtotime($item->end_hour)),
                    'actualEndDate' => $item->actual_end_date
                ];
            });

            foreach ($tasks as $key => $task) {

                if (is_null($task['actualEndDate'])) {
                    if ($currentDate <= $task['endDate']) {
                        $notFinishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                } else {
                    if ($task['actualEndDate'] >= $task['startDate'] && $task['actualEndDate'] <= $task['endDate']) {
                        $finishedTasks[] = $task;
                    } else {
                        $delayedTasks[] = $task;
                    }
                }

            }

            $data = [
                'finishedTasks'    => $finishedTasks,
                'delayedTasks'     => $delayedTasks,
                'notFinishedTasks' => $notFinishedTasks
            ];

            return $this->sendResponse($data, 'Getting tasks successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function getMostVehicleFailure($companyId) {

        $request = new Request(['companyId' => $companyId]);

        $request->validate([
            'companyId' => 'required|exists:companies,id'
        ]);

        try {

            $data = DB::table('vehicles')
                    ->join('companies', 'vehicles.company_id', '=', 'companies.id')
                    ->join('visits', 'vehicles.id', '=', 'visits.vehicle_id')
                    ->join('visit_attribute', 'visits.id', '=', 'visit_attribute.id')
                    ->join('item_failures', 'visit_attribute.id', '=', 'item_failures.visit_attribute_id')
                    ->where('companies.id', $companyId)
                    ->select(DB::raw(
                        "
                            vehicles.id as id,
                            vehicles.serial_number,
                            COUNT(vehicles.id) as failures
                        "
                    )
                    )
                    ->groupBy('vehicles.id', 'vehicles.serial_number')
                    ->orderBy('failures', 'DESC')
                    ->get();

            return $this->sendResponse($data, 'Getting all failures by vehicle succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

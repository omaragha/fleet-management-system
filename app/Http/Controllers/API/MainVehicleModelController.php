<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\MainVehicleModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainVehicleModelController extends BaseController
{

    public function index() {

        try {
            $mainVehicleModels = MainVehicleModel::all(['id', 'name']);
            return $this->sendResponse($mainVehicleModels, 'Getting main vehicle models successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'name' => 'required|unique:main_vehicle_models,name'
        ]);

        try {

            $mainVehicleModel = MainVehicleModel::create($request->all());

            return $this->sendResponse(['id' => $mainVehicleModel->id], 'Main vehicle model created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($mainVehicleModelId) {

        $request = new Request(['mainVehicleModelId' => $mainVehicleModelId]);

        $request->validate([
            'mainVehicleModelId' => 'required|exists:main_vehicle_models,id'
        ]);

        try {
            $mainVehicleModel = MainVehicleModel::find($mainVehicleModelId, ['id', 'name']);
            return $this->sendResponse($mainVehicleModel, 'Getting main vehicle model successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $mainVehicleModelId) {

        $request->merge(['mainVehicleModelId' => $mainVehicleModelId]);

        $request->validate([
            'mainVehicleModelId' => 'required|exists:main_vehicle_models,id',
            'name'               => 'unique:main_vehicle_models,name,' . $mainVehicleModelId
        ]);

        try {

            $mainVehicleModel = MainVehicleModel::find($mainVehicleModelId);

            $mainVehicleModel->name = $request->name;

            $mainVehicleModel->save();

            return $this->sendResponse([], 'Main vehicle model updated successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($mainVehicleModelId) {

        $request = new Request(['mainVehicleModelId' => $mainVehicleModelId]);

        $request->validate([
            'mainVehicleModelId' => 'required|exists:main_vehicle_models,id'
        ]);

        try {

            $mainVehicleModel = MainVehicleModel::find($mainVehicleModelId);

            $mainVehicleModel->delete();

            return $this->sendResponse([], 'Main vehicle model deleted successfully');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function getCompanyVehicleModelsByMainModel($mainVehicleModelId) {

        $request = new Request(['mainVehicleModelId' => $mainVehicleModelId]);

        $request->validate([
            'mainVehicleModelId' => 'required|exists:main_vehicle_models,id'
        ]);

        try {

            $companyVehicleModels = DB::table('company_vehicle_model')
                                    ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                                    ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
                                    ->where('company_vehicle_model.company_id', auth()->user()->company->id)
                                    ->where('main_vehicle_models.id', $mainVehicleModelId)
                                    ->get(['company_vehicle_model.id', 'vehicle_models.name']);

            return $this->sendResponse($companyVehicleModels, 'Getting vehicle models successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }


    }

}

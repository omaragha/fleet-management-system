<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Rule;
use Exception;
use Illuminate\Http\Request;

class RuleController extends BaseController
{

    public function create(Request $request) {

        $request->validate([
            'value'       => 'required',
            'attributeId' => 'required|exists:attributes,id',
            'operationId' => 'required|exists:operations,id'
        ]);

        try {

            $rule = Rule::create([
                'value'        => $request->value,
                'attribute_id' => $request->attributeId,
                'operation_id' => $request->operationId
            ]);

            $data = [
                'id' => $rule->id,
                'value' => $rule->value,
                'operationId'=>$rule->operation_id,
                'attributeId' => $rule->attribute_id
            ];

            return $this->sendResponse($data, 'Rule added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $ruleId) {

        $request->validate([
            'value' => 'required'
        ]);

        try {

            $rule = Rule::find($ruleId);

            if (!$rule) {
                throw new Exception('Rule NOT found!', 404);
            }

            $rule->value = $request->value;

            $rule->save();

            return $this->sendResponse([], 'Rule updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function delete($ruleId) {

        try {

            $rule = Rule::find($ruleId);

            if (!$rule) {
                throw new Exception('Rule NOT found!', 404);
            }

            $rule->delete();

            return $this->sendResponse([], 'Rule deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

}

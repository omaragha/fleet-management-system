<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Sparepart;
use App\Models\SparepartType;
use App\Models\Visit;
use App\Traits\ImageTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SparepartController extends BaseController
{

    use ImageTrait;

    public function index(Request $request) {

        if($request->type) {

            $spareparts = $this->getSpareparts($request->type);

        } else {

            $spareparts = $this->getSpareparts();

        }

        return $this->sendResponse($spareparts,'success');

    }

    public function create(Request $request) {

        $request->validate([
            'visitId'   => 'exists:visits,id',
            'attrId'    => 'exists:attributes,id',
            'vehicleId' => 'exists:vehicles,id',
            'issueId'   => 'exists:issues,id',
            'sparepart.sparepartTypeId' => 'required|exists:sparepart_types,id'
        ]);


        try {

            $sparepartType = SparepartType::find($request->sparepart['sparepartTypeId'])->name;

            if (!$request->has('vehicleId') && strtolower($sparepartType) == 'fuel') {
                $vehicleId = $this->getCurrentVehicleForDriver(auth()->user()->id)->getData()->data[0]->id;
                $request->merge(['vehicleId' => $vehicleId]);
            }

            $data = [
                'name'               => $request->sparepart['name'],
                'code'               => $request->sparepart['code'],
                'description'        => $request->sparepart['description'],
                'quantity'           => $request->sparepart['quantity'],
                'image'              => isset($request->sparepart['image']) ? $request->sparepart['image'] : '',
                'price'              => isset($request->sparepart['price']) ? $request->sparepart['price'] : '',
                'vehicle_id'         => $request->vehicleId,
                'sparepart_type_id'  => $request->sparepart['sparepartTypeId'],
            ];

            if ($request->has('visitId') && $request->has('attrId')) {
                $data['visit_attribute_id'] = DB::table('visit_attribute')
                                            ->where('visit_id', $request->visitId)
                                            ->where('attribute_id', $request->attrId)
                                            ->get('id')
                                            ->first()->id;
            }

            if ($request->has('issueId')) {
                $data['issue_id'] = $request->issueId;
            }

            $sparepart = Sparepart::create($data);

            return $this->sendResponse(['id' => $sparepart->id], 'Sparepart created successfully.');

        } catch (\Throwable $th) {

            return $this->sendError($th->getMessage(), 500);
        }

    }

    public function edit($sparepartId) {

        try {
            $sparepart = Sparepart::find($sparepartId, ['id', 'name', 'code', 'description', 'quantity', 'count', 'image', 'price']);

            if (!$sparepart) {
                throw new Exception('Sparepart NOT found!', 404);
            }

            return $this->sendResponse($sparepart, 'Getting Sparepart successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError('Sparepart NOT found!');
            } else {
                return $this->sendError('Internal Server Error!');
            }
        }

    }

    public function update(Request $request, $sparepartId) {

        $request->validate([
            'image' => 'image|mimes:png,jpg,jpeg'
        ]);

        try {
            $sparepart = Sparepart::find($sparepartId, ['id', 'name', 'code', 'description', 'quantity', 'count', 'image']);

            if (!$sparepart) {
                throw new Exception('Sparepart NOT found!', 404);
            }


            $sparepart->name = $request->sparepart['name'];
            $sparepart->code = $request->sparepart['code'];
            $sparepart->description = $request->sparepart['description'];
            $sparepart->quantity = $request->sparepart['quantity'];
            $sparepart->image = $request->sparepart['image'];
            $sparepart->price = $request->sparepart['price'];

            $sparepart->save();

            return $this->sendResponse([], 'Sparepart updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError('Sparepart NOT found!');
            } else {
                return $this->sendError('Internal Server Error!');
            }
        }

    }

    public function getSpareparts($sparepartType = null) {

        try {
            $spareparts = DB::table('spareparts')
            ->join('sparepart_types','sparepart_types.id','=','spareparts.sparepart_type_id')
            ->where('sparepart_types.id','=',$sparepartType)
            ->get();

            return $spareparts;

        } catch (\Throwable $th) {
            echo($th->getMessage());
            return $this->sendError('Internal Server Error!');
        }


    }

    public function delete($sparepartId) {

        try {
            $sparepart = Sparepart::find($sparepartId, ['id', 'name', 'code', 'description', 'quantity', 'count']);

            if (!$sparepart) {
                throw new Exception('Sparepart NOT found!', 404);
            }

            $sparepart->delete();

            return $this->sendResponse([], 'Sparepart deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError('Sparepart NOT found!');
            } else {
                return $this->sendError('Internal Server Error!');
            }
        }

    }

}

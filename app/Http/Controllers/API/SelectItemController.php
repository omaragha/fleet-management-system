<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\SelectItem;
use Exception;
use Illuminate\Http\Request;

class SelectItemController extends BaseController
{

    public function create(Request $request) {

        $request->validate([
            'value'       => 'required',
            'attributeId' => 'required|exists:attributes,id'
        ]);

        try {

            $selectItem = SelectItem::create([
                'value'        => $request->value,
                'attribute_id' => $request->attributeId
            ]);

            $data = [
                'id'    => $selectItem->id,
                'value' => $selectItem->value
            ];

            return $this->sendResponse($data, 'Select item added successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $selectItemId) {

        $request->validate([
            'value' => 'required'
        ]);

        try {

            $selectItem = SelectItem::find($selectItemId);

            if (!$selectItem) {
                throw new Exception('Select item NOT found!', 404);
            }

            $selectItem->value = $request->value;

            $selectItem->save();

            return $this->sendResponse([], 'Select item updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }
    }

    public function delete($selectItemId) {

        try {

            $selectItem = SelectItem::find($selectItemId);

            if (!$selectItem) {
                throw new Exception('Select NOT found!', 404);
            }

            $selectItem->delete();

            return $this->sendResponse([], 'Select item deleted successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

}

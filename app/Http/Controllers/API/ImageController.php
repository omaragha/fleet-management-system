<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Image;
use App\Traits\ImageTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageController extends BaseController
{

    use ImageTrait;

    public function create(Request $request) {

        try {

            $images = [];

            DB::beginTransaction();

            foreach ($request->all() as $key => $image) {

                if ($key == 'total') {
                    continue;
                }

                $result = $this->addImage($image, 'images');

                if ($result['status'] != 200) {
                    throw new Exception($result['message'], $result['status']);
                }

                $images[] = $result['path'];
            }

            DB::commit();

            return $this->sendResponse($images, 'Images added successfully.');
        } catch (\Throwable $th) {
            DB::rollback();
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function linkImageWithVisitAttributeId(Request $request) {

        try {

            $result = $this->deleteImages($request);

            if ($result['status'] != 200) {
                throw new Exception($result['message']);
            }

            foreach ($request->images as $key => $image) {

                $img = Image::create([
                    'path' => $image
                ]);

                $img->visit_attribute_id = $request->visitAttributeId;

                $img->save();
            }

            return $this->sendResponse([], 'Images linked with vitist attribute successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function deleteImages(Request $request) {

        try {

            $images = Image::where('visit_attribute_id', $request->visitAttributeId)->delete();

            return [
                'status'  => 200,
                'message' => 'Images deleted successfully.'
            ];
        } catch (\Throwable $th) {
            return [
                'status'  => 500,
                'message' => 'Internal Server Error!'
            ];
        }

    }

}

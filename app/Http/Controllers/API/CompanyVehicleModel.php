<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Company;
use App\Models\VehicleModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyVehicleModel extends BaseController
{

    public function index() {

        try {

            $data = Company::where('id', auth()->user()->company->id)->with('vehicleModels')->get(['id', 'name', 'location'])->first();

            if (!$data) {
                throw new Exception('Company NOT found!', 404);
            }

            $companyVehicleModels = [];

            foreach ($data->vehicleModels as $key => $companyVehicleModel) {

                $companyVehicleModels[] = [
                    'id'   => $companyVehicleModel['pivot']['id'],
                    'name' => $companyVehicleModel['name'],
                ];

            }

            return $this->sendResponse($companyVehicleModels, 'Getting company vehicle models successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

    }

    public function create(Request $request) {

        $request->validate([
            'vehicleModelId' => 'required|array',
            'vehicleModelId.*' => 'exists:vehicle_models,id'
        ]);

        try {

            $company = Company::find(auth()->user()->company->id);

            $company->vehicleModels()->syncWithoutDetaching($request->vehicleModelId);

            return $this->sendResponse([], 'Company model added successfully.');
        } catch (\Throwable $th) {
            return $th->getMessage();
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($companyVehicleModelId) {

        try {

            $company = Company::find(auth()->user()->company->id);

            $company->vehicleModels()->wherePivot('id', $companyVehicleModelId)->detach();

            return $this->sendResponse([], 'Company vehicle model deleted successfully.');

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

}

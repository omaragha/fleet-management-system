<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Models\VehicleModel;
use Exception;
use Illuminate\Support\Facades\DB;

class VehicleModelController extends BaseController
{

    public function index(Request $request)
    {
        try {
            $vehicleModels = VehicleModel::all(['id', 'name', 'type']);
            return $this->sendResponse($vehicleModels, 'Getting vehicle models successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function create(Request $request)
    {

        $request->validate([
            'name'               => 'required',
            'type'               => 'required',
            'mainVehicleModelId' => 'required|exists:main_vehicle_models,id'
        ]);

        try {

            $request = $this->fixRequest($request);

            VehicleModel::create($request->all());
            return $this->sendResponse([], 'Vehicle Model created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }
    }

    public function edit($vehicleModelId) {

        try {

            $vehicleModel = VehicleModel::find($vehicleModelId, ['id', 'name', 'type']);

            if (!$vehicleModel) {
                throw new Exception('Vehicle model NOT found!', 404);
            }

            return $this->sendResponse($vehicleModel, 'Getting vehicle model info successfully.');
        } catch (\Throwable $th) {

            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function update(Request $request, $vehicleModelId) {

        $request->validate([
            'name' => 'required',
            'type' => 'required'
        ]);

        try {

            $vehicleModel = VehicleModel::find($vehicleModelId);

            if (!$vehicleModel) {
                throw new Exception('Vehicle Model NOT found!', 404);
            }

            $vehicleModel->name = $request->name;
            $vehicleModel->type = $request->type;
            $vehicleModel->save();

            return $this->sendResponse([], 'Vehicle Model updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function delete($vehicleModelId) {

        try {

            $vehicleModel = VehicleModel::find($vehicleModelId);

            if (!$vehicleModel) {
                throw new Exception('Vehicle Model NOT found!', 404);
            }

            $vehicleModel->delete();

            return $this->sendResponse('Vehicle Model deleted successfully.');

        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }
}

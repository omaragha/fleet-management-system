<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Models\Equipement;
use Illuminate\Support\Facades\DB;

class EquipmentsController extends BaseController
{
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required'            
        ]);

        try {
            Equipement::create($request->all());
            return $this->sendResponse([], 'Equipement created successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal server error.', 500);
        }

    }

    public function index()
    {
        try {
            echo "hello";
            $equipement = DB::table('equipements')->get();
            return $this->sendResponse($equipement, 'Getting equipement successfully.');
            } catch (\Throwable $th) {
                return $this->sendError('Internal server error.', 500);
            }
    }
}

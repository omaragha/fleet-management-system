<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Imports\VisitImport;
use App\Models\Checklist;
use App\Models\ChecklistType;
use App\Models\Image;
use App\Models\ItemFailure;
use App\Models\Sparepart;
use App\Models\Status;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Excel;
use Exception;

class VisitController extends BaseController
{

    // public function index()
    // {

    //     $visits = [];

    //     $conditions = [];

    //     try {

    //         $result = $this->updateStatusOfVisits();

    //         if ($result->getStatusCode() != 200) {
    //             throw new Exception('Internal Server Error!', 500);
    //         }

    //         // admin tasks.
    //         if (auth()->user()->role->id == 1) {

    //             $conditions[] = ['companies.id', auth()->user()->company->id];

    //             if (request('type')) {
    //                 $typeId = ChecklistType::where('name', request('type'))->get()->first();
    //                 if ($typeId) {
    //                     $conditions[] = ['visits.checklist_type_id', $typeId->id];
    //                 } else {
    //                     return $this->sendResponse([], 'Getting visits successfully.');
    //                 }
    //             } else if (request('year') && request('month')) {
    //                 $start = request('year') . '-' . request('month') . '-1';
    //                 $end   = request('year') . '-' . request('month') . '-' . cal_days_in_month(CAL_GREGORIAN, request('month'), request('year'));
    //                 $conditions[] = ['visits.start_date', '>=', $start];
    //                 $conditions[] = ['visits.end_date', '<=', $end];
    //             } else {
    //                 return $this->sendResponse([], 'Getting visits successfully.');
    //             }

    //             $visits = [];

    //             if (count($conditions) == 2) {
    //                 $visits = $this->getQuery('admin', 'type', $conditions);
    //             } else {
    //                 array_pop($conditions);
    //                 array_pop($conditions);
    //                 $visits = $this->getQuery('admin', '', $conditions, $start, $end);
    //             }

    //         } else {

    //             $conditions = [
    //                 ['user_id', auth()->user()->id],
    //                 ['status', '!=', 'black'],
    //                 ['status', '!=', 'green']
    //             ];

    //             if (request('type')) {
    //                 $typeId = ChecklistType::where('name', request('type'))->get()->first();
    //                 if ($typeId) {
    //                     $conditions[] = ['checklist_type_id', $typeId->id];
    //                 } else {
    //                     return $this->sendResponse([], 'Getting visits successfully.');
    //                 }
    //             } else if (request('year') && request('month')) {
    //                 $start = strtotime(request('year') . '-' . request('month') . '-1');
    //                 $end   = strtotime(request('year') . '-' . request('month') . '-' . cal_days_in_month(CAL_GREGORIAN, request('month'), request('year')));
    //                 $conditions[] = ['visits.start_date', '>=', $start];
    //                 $conditions[] = ['visits.end_date', '<=', $end];
    //             } else {
    //                 return $this->sendResponse([], 'Getting visits successfully.');
    //             }

    //             if (count($conditions) == 4) {
    //                 $data = $this->getQuery('other', 'type', $conditions);
    //             } else {
    //                 array_pop($conditions);
    //                 array_pop($conditions);
    //                 $data = $this->getQuery('other', '', $conditions, $start, $end);
    //             }

    //             $data = $this->getQuery('other',);

    //             foreach ($data as $key => $visit) {
    //                 $visits[] = [
    //                     'vehicleName'  => $visit->vehicle->name,
    //                     'location'     => $visit->location,
    //                     'startDate'    => date('Y-m-d h:i:sa', strtotime($visit->start_date)),
    //                     'endDate'      => date('Y-m-d h:i:sa', strtotime($visit->end_date)),
    //                     'status'       => $visit->status
    //                 ];
    //             }
    //         }
    //     } catch (\Throwable $th) {
    //         return $this->sendError('Internal Server Error!', 500);
    //     }


    //     return $this->sendResponse($visits, 'Getting visits successfully.');
    // }

    public function calender(Request $request) {

        try {

            $conditions = [];

            if (auth()->user()->role->id != 1) {
                $conditions[] = [
                    'user_id', auth()->user()->id
                ];
            }


            $year = date('Y');

            $month = date('m');

            if ($request->has('year')) {
                $year = $request->year;
            }

            if ($request->has('month')) {
                $month = $request->month;
            }

            $startDate = $year . '-' . $month . '-01';

            $endDate = $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year);


            // return [
            //     $request->all(),
            //     $startDate,
            //     $endDate
            // ];

            $visits = Visit::where($conditions)
                    // ->whereDate('start_date', '<=', $endDate)
                    // ->whereDate('end_date', '>=', $startDate)
                    ->whereIn('status', ['pending', 'running'])
                    ->with('vehicle', 'user', 'checklistType')
                    ->get()
                    ->map(function ($item) {

                        $actualDate = strtotime($item->actual_date);
                        $startDate = strtotime($item->start_date);
                        $endDate =  strtotime($item->end_date);

                        $currentDate = time();



                        if(($currentDate <= $startDate)  && ($currentDate <= $endDate )) {

                            $item['visitStatus'] = "available"; //correct

                        } else if((!$actualDate) && $currentDate >=$endDate) {

                            $item['visitStatus'] = "overdueNotVisited"; //overdue and not visited yet

                        } else if($actualDate && $currentDate >=$endDate) {

                            $item['visitStatus'] = "overdueVisited"; //overdue and visited
                        }


                        return [
                            'id'          => $item->id,
                            'stauts'      => $item->status,
                            'startDate'   => date('Y-m-d', strtotime($item->start_date)),
                            'endDate'     => date('Y-m-d', strtotime($item->end_date)),
                            'vehicleId'   => $item->vehicle->id,
                            'vehicleName' => $item->vehicle->name,
                            'visitType'   => $item->checklistType->name,
                            'visitStatus' => $item->visitStatus
                        ];
                    });

            return $this->sendResponse($visits, 'Getting visits successfully.');
        } catch (\Throwable $th) {

            return $th->getMessage();
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function index() {


        try {

            $conditions[]=[
                'company_vehicle_model.company_id',auth()->user()->company->id
            ];

            // if(auth()->user()->role_id != 1) {
            //     $conditions[] = [
            //         'users.id', auth()->user()->id
            //     ];
            // }

            $statues = [];

            if (auth()->user()->role_id != 1) {

                if (auth()->user()->role_id == 2) {
                    $statues  = ['to-be-checked','cantVisit'];
                }

                if (auth()->user()->role_id == 3) {

                    $statues  = ['pending','running','re-check'];

                }

            }


            if (request()->has('type')) {
                if (strlen(request()->type) != 0) {
                    $conditions[] = ['checklist_types.name', request()->type];
                }
            }

            //dd($conditions);
            $visits = DB::table('visits')
                    ->join('users', 'visits.user_id', '=', 'users.id')
                    ->join('checklist_types', 'visits.checklist_type_id', '=', 'checklist_types.id')
                    ->join('vehicles', 'visits.vehicle_id', '=', 'vehicles.id')
                    ->join('company_vehicle_model', 'vehicles.company_vehicle_model_id', '=', 'company_vehicle_model.id')
                    ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                    ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
                    ->where($conditions)
                    ->whereIn('visits.status',$statues)
                    ->select(DB::raw("
                        visits.id,
                        visits.status,
                        vehicles.name as vehicleName,
                        visits.start_date as startDate,
                        visits.end_date as endDate,
                        CONCAT(main_vehicle_models.name, ' ', vehicle_models.name) as vehicleType,
                        CONCAT(users.first_name, ' ', users.last_name) as operator
                    "))
                    ->get();

            return $this->sendResponse($visits, 'Getting visits succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage(), 500);
        }


    }

    public function view(Request $request , $id) {

        $request->merge(['visitId' => $id]);

        $request->validate([
            'visitId' => 'required|exists:visits,id'
        ]);

        try {

            if(!$id) {
                $this->sendError('visit is not exsit');
                return;
            }



           $visit =  DB::table('visits')
            ->join('vehicles','vehicles.id','=','visits.vehicle_id')
            ->join('company_vehicle_model','company_vehicle_model.id','=','vehicles.company_vehicle_model_id')
            ->join('vehicle_models','vehicle_models.id','=','company_vehicle_model.vehicle_model_id')
            ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
            ->join('checklist_types','checklist_types.id','=','visits.checklist_type_id')
            ->where('visits.id',$id)
            ->get([
                'visits.id',
                'vehicles.id as vehicleId',
                'vehicles.name',
                'vehicle_models.name as make',
                'main_vehicle_models.name as model',
                'serial_number as serialNumber',
                'license_plate as licensePlate',
                'visits.start_date as startDate',
                'visits.end_date as endDate',
                'checklist_types.name as checklistType'
            ])
            ->first();

            $vehicleId = $visit->vehicleId;

            $this->updateVisitStatus($visit->id,'running');

            $visit->lastVisitDate = $this->getLastVisit($vehicleId);

            return $this->sendResponse($visit,'Getting data successfully');


        } catch (\Throwable $th) {


            return $this->sendError('Internal Server Error!', 500);

        }





    }


    public function updateVisitStatus($visitId , $status) {

        try {

            $visit = Visit::find($visitId);

            $visit->status = $status;

            $visit->save();

        } catch (\Throwable $th) {
            return $this->sendError($th->getMessage(), 500);
        }

    }

    public function getLastVisit($vehicleId) {


        $conditions = [];

        try {
            $visit = Visit::with('vehicle')
            ->whereHas('vehicle',function($query) use ($vehicleId){

                $query->where('id',$vehicleId);

            })
            ->where('actual_date','!=',null)
            ->orderByDesc('start_date')
            ->limit(2)
            ->get()
            ->map(function($visit) {

                return $visit->start_date;

            });

            if($visit[1]) {

                return $visit[1];

            } else {

                return null;

            }

        } catch (\Throwable $th) {

            $th->getMessage();

            return null;

        }

    }

    public function create(Request $request)
    {

        $headers = [
            'vehiclecode',
            'employeecode',
            'startdate',
            'enddate',
            'location'
        ];

        $request->validate([
            'file' => 'required|mimes:xlsx, xls, csv',
            'type' => 'required|exists:checklist_types,id'
        ]);

        try {

            DB::beginTransaction();

            $array = Excel::toArray(new VisitImport, $request->file('file'));

            foreach ($array as $key => $row) {
                foreach ($row as $key1 => $col) {
                    foreach ($headers as $key2 => $header) {
                        if (!isset($col[$header]) || (isset($col[$header]) && empty($col[$header]))) {
                            throw new Exception('Invalid data, Please reupload the file!', 422);
                        }
                    }

                    if (
                        !$this->checkIdIfExistsOrNot($col['vehiclecode'], 'vehicles') ||
                        !$this->checkIdIfExistsOrNot($col['employeecode'], 'users') ||
                        !$this->checkIdIfExistsOrNot($col['type'], 'checklist_types')
                    ) {
                        throw new Exception('Invalid data, Please reupload the file!', 422);
                    }

                    Visit::create([
                        'vehicle_id'        => $col['vehiclecode'],
                        'user_id'           => $col['employeecode'],
                        'checklist_type_id' => $request->type,
                        'start_date'        => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($col['startdate']),
                        'end_date'          => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($col['enddate']),
                        'location'          => $col['location']
                    ]);

                }
            }

            DB::commit();

            return $this->sendResponse([], 'Visits added successfully.');
        } catch (\Throwable $th) {
            DB::rollBack();
            if ($th->getCode() == 422) {
                return $this->sendError($th->getMessage(), 422);
            } else {
                return $this->sendError('Internal Server Error', 500);
            }
        }
    }

    public function checkIdIfExistsOrNot($id, $type) {

        $result = false;

        switch ($type) {
            case 'vehicles':
                $result = Vehicle::find($id) ?? true;
                break;
            case 'users':
                $result = User::find($id) ?? true;
                break;
            case 'checklist_types':
                $result = ChecklistType::find($id) ?? true;
                break;
            default:
                break;
        }

        return $result;
    }

    public function updateStatusOfVisits() {

        try {

            $visits = Visit::where('status', '!=', 'green')->get();

            $today = strtotime('today');

            foreach ($visits as $key => $visit) {

                $startDate = strtotime($visit->start_date);
                $endDate = strtotime($visit->end_date);

                if ($today >= $startDate && $today <= $endDate) {
                    $visit->status = 'blue';
                } else if ($today > $endDate) {
                    $visit->status = 'red';
                }
                $visit->save();
            }

            return $this->sendResponse([], 'Visit status updated successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function finishTask(Request $request,$visitId) {

        // $request = new Request(['visitId' => $visitId]);

        $request->merge(['visitId'=>$visitId]);

        $request->validate([
            'visitId' => 'required|exists:visits,id'
        ]);

        $visit = Visit::find($visitId);

        $visit->actual_date = date('Y-m-d H:i:s');

        //$visit->status = $request->type;


        $roleId = auth()->user()->role_id;
        
        if($roleId == 3) {
            
            if($visit->status == 'running' || $visit->status == 're-check') {

                $this->changeStatus($visit->id,'to-be-checked');

            }

        }

        if($roleId == 2) {

            if($visit->status == 'to-be-checked' || $visit->status == 'cantVisit') {

                // echo 'here';
                $this->changeStatus($visit->id,'finished');

            }
        }




        if($request->type == 'cantVisit') {

            $visit->justification()->associate($request->justification);

            $visit->save();

            return $this->sendResponse([],'success');

        }

        $visit->save();

        try {

            $items = DB::table('visit_attribute')
                ->join('attributes', 'visit_attribute.attribute_id', '=', 'attributes.id')
                ->join('rules', 'attributes.id', '=', 'rules.attribute_id')
                ->join('operations', 'rules.operation_id', '=', 'operations.id')
                ->where('visit_attribute.visit_id', $visitId)
                ->get([
                    'visit_attribute.id',
                    'operations.operation',
                    'rules.value as expectedValue',
                    'visit_attribute.value'
                ]);

            foreach ($items as $key => $item) {


                switch ($item->operation) {
                    case '=':

                        if ($item->value != $item->expectedValue) {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id
                            ]);
                        }

                        break;
                    case '>':

                        if ($item->value <= $item->expectedValue) {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id,
                            ]);
                        }

                        break;
                    case '>=':

                        if ($item->value < $item->expectedValue) {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id,
                            ]);
                        }

                        break;
                    case '<':

                        if ($item->value >= $item->expectedValue) {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id,
                            ]);
                        }

                        break;
                    case '<=':

                        if ($item->value > $item->expectedValue) {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id,
                            ]);
                        }

                        break;
                    case 'Between':

                        $start = explode(',', $item->expectedValue)[0];
                        $end = explode(',', $item->expectedValue)[1];

                        if ($item->value < $start || $item->value > $end) {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id,
                            ]);
                        }

                        break;

                    case ('checkbox' || 'twoCheckbox'):

                        if ($item->value == '0') {
                            ItemFailure::create([
                                'visit_attribute_id' => $item->id,
                            ]);
                        }

                        break;

                    default:
                        # code...
                        break;
                }

            }

            return $this->sendResponse([], 'Task finished succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function rejectTask($visitId) {
        
        $visit = Visit::find($visitId);

        $visit->justification()->associate(request()->justification);
        
        $this->changeStatus($visit->id);

    }

    public function getQuery($role = 'admin', $condition = 'type', $conditions = [], $startDate = '', $endDate = '') {

        if ($condition == 'type') {

            if ($role == 'admin') {

                $visits = DB::table('visits')
                ->join('vehicles', 'visits.vehicle_id', '=', 'vehicles.id')
                ->join('companies', 'vehicles.company_id', '=', 'companies.id')
                ->join('users', 'visits.user_id', '=', 'users.id')
                ->join('checklist_types', 'visits.checklist_type_id', '=', 'checklist_types.id')
                ->where($conditions)
                ->select(
                    DB::raw("
                        visits.id,
                        vehicles.name as vehicleName,
                        CONCAT(users.first_name, ' ', users.last_name) as employeeName,
                        FORMAT(visits.start_date, 'yyyy-MM-dd hh:mm:ss tt') as startDate,
                        FORMAT(visits.end_date, 'yyyy-MM-dd hh:mm:ss tt') as endDate,
                        visits.location,
                        checklist_types.name as checklistTypeName,
                        visits.status
                    ")
                )
                ->get();

            } else {
                $visits = Visit::where($conditions)->get();
            }
        } else {

            if ($role == 'admin') {

                $visits = DB::table('visits')
                ->join('vehicles', 'visits.vehicle_id', '=', 'vehicles.id')
                ->join('companies', 'vehicles.company_id', '=', 'companies.id')
                ->join('users', 'visits.user_id', '=', 'users.id')
                ->join('checklist_types', 'visits.checklist_type_id', '=', 'checklist_types.id')
                ->where($conditions)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->where(function ($q) use ($startDate) {
                            $q->where('visits.start_date', '>=', $startDate)
                              ->whereYear('visits.start_date', date('Y', strtotime($startDate)))
                              ->whereMonth('visits.start_date', date('m', strtotime($startDate)));
                        })
                        ->orWhere(function ($q) use ($endDate) {
                            $q->where('visits.end_date', '<=', $endDate)
                              ->whereYear('visits.end_date', date('Y', strtotime($endDate)))
                              ->whereMonth('visits.end_date', date('m', strtotime($endDate)));
                        });
                })
                ->select(
                    DB::raw("
                        visits.id,
                        vehicles.name as vehicleName,
                        CONCAT(users.first_name, ' ', users.last_name) as employeeName,
                        FORMAT(visits.start_date, 'yyyy-MM-dd hh:mm:ss tt') as startDate,
                        FORMAT(visits.end_date, 'yyyy-MM-dd hh:mm:ss tt') as endDate,
                        visits.location,
                        checklist_types.name as checklistTypeName,
                        visits.status
                    ")
                )
                ->get();

            } else {
                $visits = Visit::where($conditions)
                        ->where(function ($query) use ($startDate, $endDate) {
                            $query->where(function ($q) use ($startDate) {
                                $q->where('visits.start_date', '>=', $startDate)
                                  ->whereYear('visits.start_date', date('Y', strtotime($startDate)))
                                  ->whereMonth('visits.start_date', date('m', strtotime($startDate)));
                            })
                            ->orWhere(function ($q) use ($endDate) {
                                $q->where('visits.end_date', '<=', $endDate)
                                  ->whereYear('visits.end_date', date('Y', strtotime($endDate)))
                                  ->whereMonth('visits.end_date', date('m', strtotime($endDate)));
                            });
                        })->get();
            }

        }

        return $visits;
    }

    // public function initializationVisitVehicle(Request $request, $visitId) {

    //     $request->merge([
    //         'visitId' => $visitId
    //     ]);

    //     $request->validate([
    //         'visitId' => 'required|exists:visits,id'
    //     ]);

    //     try {

    //         $visit = Visit::find($visitId);

    //         if ($visit->initialized) {
    //             return $this->sendResponse([], 'Visit attribute initialized successfully.');
    //         }

    //         $visit = Visit::where('id', $visitId)->with('vehicle')->get()->first();



    //         $companyVehicleModelId = $visit->vehicle->company_vehicle_model_id;


    //         $result = DB::table('user_checklist')
    //                        ->where('company_vehicle_model_id', $companyVehicleModelId)
    //                        ->get()
    //                        ->first();

    //                        echo $companyVehicleModelId;



    //         if ($result) {

    //             $visit = Visit::where('id', $request->visitId)->get()->first();

    //             $data = Checklist::where('id', $result->checklist_id)->with('sections', 'sections.attributes')->get()->first();

    //             foreach ($data->sections as $key => $section) {

    //                 foreach ($section->attributes as $key1 => $attribute) {

    //                     $visit->attributes()->syncWithoutDetaching($attribute->id, [
    //                         'justification' => '',
    //                         'value'         => ''
    //                     ]);

    //                 }

    //             }

    //             $visit->initialized = 1;

    //             $visit->save();

    //             $checklist = Checklist::find($result->checklist_id);

    //             $checklist->used = 1;

    //             $checklist->save();

    //         }

    //         return $this->sendResponse([], 'Visit attribute initialized successfully.');
    //     } catch (\Throwable $th) {
    //         if ($th->getCode() == 404) {
    //             return $this->sendError($th->getMessage());
    //         } else {
    //             return $this->sendError('Internal Server Error!');
    //         }
    //     }

    // }

    public function getVisitDetails(Request $request, $visitId) {

        $request->merge(['visitId' => $visitId]);

        $request->validate([
            'visitId' => 'required|exists:visits,id'
        ]);


        try {



            if (!$visitId) {
                throw new Exception('Visit NOT found!');
            }


            $result = DB::table('visits')
            ->join("vehicles","visits.vehicle_id","=","vehicles.id")
            ->where('visits.id',$visitId)
            ->get()
            ->first();
            
            $roleId = auth()->user()->role_id;

            if($roleId == 3) { 

                if($result->status == 'pending') {
            
                    $this->changeStatus($result->id,'running');

                } 
            }

            $checklistId = $result->checklist_id;

            $checklistType = ChecklistType::find($result->checklist_type_id)->name;


            $checklistInfo = Checklist::where('id', $checklistId)
            ->with('sections',
            'sections.attributes',
            'sections.attributes.rules',
            'sections.attributes.inputType',
            'sections.attributes.selectItems',
            'sections.attributes.rules.operation'
            )->get()
            ->first();


            $sections = [];

            foreach ($checklistInfo->sections as $key => $section) {

                $attributes = [];

                foreach ($section->attributes as $key1 => $attribute) {

                    $rules = [];

                    foreach ($attribute->rules as $key2 => $rule) {

                        $rules[] = [
                            'id'            => $rule->id,
                            'operationId'   => $rule->operation_id,
                            'value'         => $rule->value,
                            'operationName' => $rule->operation->operation
                        ];
                    }

                    $selectItems = [];

                    foreach ($attribute->selectItems as $key2 => $item) {
                        $selectItems[] = [
                            'id'    => $item->id,
                            'name'  => $item->value
                        ];
                    }

                    $result = DB::table('visit_attribute')
                            ->where('visit_id', $visitId)
                            ->where('attribute_id', $attribute->id)
                            ->get();

                    if (count($result)) {

                        $imgs = Image::where('visit_attribute_id', $result[0]->id)->get(['path']);

                        $images = [];

                        foreach ($imgs as $key2 => $image) {
                            $images[] = $image->path;
                        }

                        $spareparts = Sparepart::where("visit_attribute_id",$result[0]->id)
                        ->get([
                            'id',
                            'name',
                            'code',
                            'description',
                            'image',
                            'quantity'
                        ]);


                        // foreach ($result as $key2 => $visitAttribute) {

                        //     $sparepart = Sparepart::find($visitAttribute->sparepart_id);

                        //     if ($sparepart) {
                        //         $spareparts[] = [
                        //             'id'          => $sparepart->id,
                        //             'name'        => $sparepart->name,
                        //             'code'        => $sparepart->code,
                        //             'description' => $sparepart->description,
                        //             'image'       => $sparepart->image,
                        //             'quantity'    => $sparepart->quantity
                        //         ];
                        //     }


                        // }

                        $attributes[] = [
                            'id'                => $attribute->id,
                            'name'              => $attribute->name,
                            'value'             => $result[0]->value,
                            'justification'     => $result[0]->justification,
                            'images'            => $images,
                            'spareparts'        => $spareparts,
                            'validationValid'   => $attribute->validation_valid,
                            'validationInvalid' => $attribute->validation_invalid,
                            'inputType'         => $attribute->inputType->name,
                            'rules'             => $rules,
                            'selectItems'       => $selectItems,
                            'isRequired'        => $attribute->is_required,
                            'sparepartTypeId' =>$attribute->sparepart_type_id
                        ];

                    }

                }

                if (count($attributes)) {
                    $sections[] = [
                        'id'         => $section->id,
                        'name'       => $section->name,
                        'attributes' => $attributes
                    ];
                }


            }

            $data = [
                'id'       => $checklistId,
                'name'     => $checklistInfo->name,
                'type'     => $checklistType,
                'sections' => $sections
            ];

            return $this->sendResponse($data, 'Getting visit details successfully.');
        } catch (\Throwable $th) {
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }

        }

    }

    public function visitVehicle(Request $request) {

        $request->validate([
            'data'            => 'array',
            'data.*.id'       => 'required|exists:attributes,id',
            'data.*.images'   => 'array',
            'visitId'         => 'required|exists:visits,id',
        ]);

        try {

            DB::beginTransaction();

            foreach ($request->data as $key => $value) {

                $visitAttribute = DB::table('visit_attribute')
                    ->where('visit_id', $request->visitId)
                    ->where('attribute_id', $value['id'])
                    ->get()
                    ->first();

                DB::table('visit_attribute')
                ->where('visit_id', $request->visitId)
                ->where('attribute_id', $value['id'])
                ->update([
                    'justification' => $value['justification'],
                    'value'         => $value['value']
                ]);

                $visitAttributeId = $visitAttribute->id;

                if (isset($value['images'])) {

                    $request->merge([
                        'visitAttributeId' => $visitAttributeId,
                        'images'           => $value['images']
                    ]);

                    $result = (new ImageController())->linkImageWithVisitAttributeId($request);

                    if ($result->getStatusCode() != 200) {
                        throw new Exception($result->getData()->message, $result->getStatusCode());
                    }

                }
            }


            DB::commit();

            return $this->sendResponse([], 'Vehicle visited successfully.');
        } catch (\Throwable $th) {
            DB::rollback();
            if ($th->getCode() == 404) {
                return $this->sendError($th->getMessage());
            } else {
                return $this->sendError('Internal Server Error!', 500);
            }
        }

    }

    public function getSparepartsByVisitId($visitId) {

        $request = new Request(['visitId' => $visitId]);

        $request->validate([
            'visitId' => 'required|exists:visits,id'
        ]);

        try {

            $data = DB::table('visit_attribute')
                    ->join('spareparts', 'visit_attribute.sparepart_id', '=', 'spareparts.id')
                    ->where('visit_attribute.visit_id', $visitId)
                    ->get([
                        'spareparts.id',
                        'spareparts.name',
                        'spareparts.code',
                        'spareparts.description',
                        'spareparts.quantity',
                        'spareparts.image'
                    ]);


            return $this->sendResponse($data, 'Getting spareparts by visit successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function checkAttributesOfVisit($visitId) {

        $request = new Request(['visitId' => $visitId]);

        $request->validate([
            'visitId' => 'required|exists:visits,id'
        ]);

        try {

            $items = DB::table('visit_attribute')
                ->join('attributes', 'visit_attribute.attribute_id', '=', 'attributes.id')
                ->join('rules', 'attributes.id', '=', 'rules.attribute_id')
                ->join('operations', 'rules.operation_id', '=', 'operations.id')
                ->get();

            return $items;

        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function visitsReport(){

        try {

            $conditions[]=[
                'company_vehicle_model.company_id',auth()->user()->company->id
            ];

            $visits = DB::table('visits')
                    ->join('users', 'visits.user_id', '=', 'users.id')
                    ->join('checklist_types', 'visits.checklist_type_id', '=', 'checklist_types.id')
                    ->join('vehicles', 'visits.vehicle_id', '=', 'vehicles.id')
                    ->join('company_vehicle_model', 'vehicles.company_vehicle_model_id', '=', 'company_vehicle_model.id')
                    ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                    ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
                    ->where($conditions)
                    ->select(DB::raw("
                        visits.id,
                        visits.status,
                        FORMAT(visits.start_date, 'yyyy-MM-dd') as startDate,
                        FORMAT(visits.end_date, 'yyyy-MM-dd') as endDate,
                        CONCAT(main_vehicle_models.name, ' ', vehicle_models.name) as vehicleName,
                        CONCAT(users.first_name, ' ', users.last_name) as operator
                    "))
                    ->get();

            return $this->sendResponse($visits, 'Getting visits succesfully.');
        } catch (\Throwable $th) {

            echo $th->getMessage();

            return $this->sendError('Internal Server Error!', 500);
        }
    }



    public function changeStatus($visitId,$status="pending") {

        $visit = Visit::find($visitId);

        Status::create([
            "name"     =>  $status,
            "visit_id" =>  $visitId
        ]);
        
        
        $visit->status = $status;

        $visit->save();


    }


    public function getVisitStatus($visitId) {


        try {
            
            $status = Status::where('visit_id',$visitId)
            ->get();
            
            return $this->sendResponse($status,'success');

        } catch (\Throwable $th) {
            //throw $th;

            return $this->sendError($th->getMessage(),500);

        }
        


    }
}

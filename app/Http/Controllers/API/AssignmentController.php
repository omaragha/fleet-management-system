<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Assignment;
use Exception;
use Illuminate\Http\Request;

class AssignmentController extends BaseController
{

    public function index() {

        try {

            $assignments = Assignment::where('actual_end_date', null)->with('user', 'vehicle')->get()->map(function ($item) {

                return [
                    'id'              => $item->id,
                    'startDate'       => date('Y-m-d', strtotime($item->start_date)),
                    'endDate'         => date('Y-m-d', strtotime($item->end_date)),
                    'startHour'       => $item->start_hour,
                    'endHour'         => $item->end_hour,
                    'actual_end_date' => $item->actual_end_date,
                    'comments'        => $item->comments,
                    'operatorId'      => $item->user->id,
                    'operator'        => $item->user->first_name . ' ' . $item->user->last_name,
                    'vehicleId'       => $item->vehicle->id,
                    'vehicleName'     => $item->vehicle->name,
                    'serialNumber'    => $item->vehicle->serial_number
                ];

            });

            return $this->sendResponse($assignments, 'Getting assignments succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function create(Request $request) {

        $request->validate([
            'startDate' => 'date',
            'endDate'   => 'date',
            'vehicleId' => 'required|exists:vehicles,id',
            'userId'    => 'required|exists:users,id'
        ]);

        try {

            $currentDriver = $this->getCurrentDriverForVehicle($request->vehicleId);

            if ($currentDriver->getStatusCode() != 200) {
                throw new Exception('Internal Server Error!', 500);
            }

            if (count((array) $currentDriver->getData()->data)) {
                return $this->sendError('Sorry, this vehicle is assigned to another user now.', 422);
            }

            $data = [
                'start_date' => $request->startDate,
                'end_date'   => $request->endDate,
                'user_id'    => $request->userId,
                'vehicle_id' => $request->vehicleId,
            ];

            if ($request->has('startHour')) {
                $data['start_hour'] = date('Y-m-d H:i:s', strtotime($request->startHour));
            }

            if ($request->has('endHour')) {
                $data['end_hour'] = date('Y-m-d H:i:s', strtotime($request->startHour));
            }

            if ($request->has('comments')) {
                $data['comments'] = $request->comments;
            }

            $assignment = Assignment::create($data);

            return $this->sendResponse($assignment, 'Assignment created succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function edit($assignmentId) {

        $request = new Request(['assignmentId' => $assignmentId]);

        $request->validate([
            'assignmentId' => 'required|exists:assignments,id'
        ]);

        try {

            $assignment = Assignment::with('user', 'vehicle')->get()->map(function ($item) {
                return [
                    'id'              => $item->id,
                    'startDate'       => date('Y-m-d', strtotime($item->start_date)),
                    'endDate'         => date('Y-m-d', strtotime($item->end_date)),
                    'startHour'       => $item->start_hour,
                    'endHour'         => $item->end_hour,
                    'actual_end_date' => $item->actual_end_date,
                    'comments'        => $item->comments,
                    'operatorId'      => $item->user->id,
                    'operator'        => $item->user->first_name . ' ' . $item->user->last_name,
                    'vehicleId'       => $item->vehicle->id,
                    'vehicle'         => $item->vehicle->name,
                    'serialNumber'    => $item->vehicle->serial_number
                ];
            })->first();

            return $this->sendResponse($assignment, 'Getting assignment succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function update(Request $request, $assignmentId) {

        $request->merge(['assignmentId' => $assignmentId]);

        $request->validate([
            'assignmentId'    => 'required|exists:assignments,id',
            'startDate'       => 'date',
            'endDate'         => 'date',
            'actual_end_date' => 'date',
            'vehicleId'       => 'required|exists:vehicles,id',
            'userId'          => 'required|exists:users,id'
        ]);

        try {

            $assignment = Assignment::find($assignmentId);

            $assignment->start_date = $request->startDate;
            $assignment->end_date = $request->endDate;
            $assignment->start_hour = $request->has('startHour') ? date('Y-m-d H:i:s', strtotime($request->startHour)) : null;
            $assignment->end_hour = $request->has('endHour') ? date('Y-m-d H:i:s', strtotime($request->endHour)) : null;
            $assignment->actual_end_date = $request->actual_end_date;

            if ($request->has('vehicleId')) {
                $assignment->vehicle_id = $request->vehicleId;
            }

            if ($request->has('userId')) {
                $assignment->user_id = $request->userId;
            }

            $assignment->save();

            return $this->sendResponse($assignment, 'Assignment updated succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function delete($assignmentId) {

        $request = new Request(['assignmentId' => $assignmentId]);

        $request->validate([
            'assignmentId' => 'required|exists:assignments,id'
        ]);

        try {
            Assignment::find($assignmentId)->delete();
            return $this->sendResponse([], 'Assignment deleted succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }

    public function finishAssignment($assignmentId) {

        $request = new Request(['assignmentId' => $assignmentId]);

        $request->validate([
            'assignmentId' => 'required|exists:assignments,id'
        ]);

        try {

            $assignment = Assignment::find($assignmentId);

            $assignment->actual_end_date = date('Y-m-d H:i:s');

            $assignment->save();

            return $this->sendResponse([], 'Assignment finished succesfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Internal Server Error!', 500);
        }

    }


}

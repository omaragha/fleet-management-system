<?php

namespace App\Traits;

use App\Models\Image;
use Exception;
use GuzzleHttp\Psr7\Request;

trait ImageTrait {

    public function addImage($image, $folderName) {

        try {

            if (!file_exists(public_path('/storage/' . $folderName))) {
                mkdir(public_path('/storage/' . $folderName), 0777, true);
            }

            $name = md5($image->getClientOriginalName() . round(microtime(true) * 1000)) . '.' . $image->getClientOriginalExtension();

            $image->move(public_path('/storage/' . $folderName), $name);

            return [
                'path'    => $folderName . '/' . $name,
                'status'  => 200,
                'message' => 'Image added successfully.'
            ];
        } catch (\Throwable $th) {
            return [
                'status'  => 500,
                'message' => 'Internal Server Error!'
            ];
        }

    }

}



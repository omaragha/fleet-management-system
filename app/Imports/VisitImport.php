<?php

namespace App\Imports;

use App\Models\Visit;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VisitImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Visit([
            'vehicle_id'        => $row['vehiclecode'],
            'user_id'           => $row['employeecode'],
            'location'          => $row['location'],
            'start_date'        => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['startdate']),
            'end_date'          => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['enddate']),
            'checklist_type_id' => $row['type'],
        ]);
    }
}

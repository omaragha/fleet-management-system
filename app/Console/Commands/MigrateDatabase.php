<?php

namespace App\Console\Commands;

use App\Models\DW\DimCompany;
use App\Models\DW\DimDate;
use App\Models\DW\DimModel;
use App\Models\DW\DimSparepart;
use App\Models\DW\DimSparepartType;
use App\Models\DW\DimVehicle;
use App\Models\DW\FactMaintenance;
use App\Models\Sparepart;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MigrateDatabase:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate database to start schema.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->migrateDatabaseToStarSchema();
    }

    public function migrateDatabaseToStarSchema() {

        $month = date('m', strtotime('-1 month'));

        $year = date('Y');

        $data = DB::table('spareparts')
                ->join('sparepart_types', 'spareparts.sparepart_type_id', '=', 'sparepart_types.id')
                ->join('vehicles', 'spareparts.vehicle_id', '=', 'vehicles.id')
                ->join('companies', 'vehicles.company_id', '=', 'companies.id')
                ->join('company_vehicle_model', 'vehicles.company_vehicle_model_id', '=', 'company_vehicle_model.id')
                ->join('vehicle_models', 'company_vehicle_model.vehicle_model_id', '=', 'vehicle_models.id')
                ->join('main_vehicle_models', 'vehicle_models.main_vehicle_model_id', '=', 'main_vehicle_models.id')
                ->whereYear('spareparts.created_at', $year)
                ->whereMonth('spareparts.created_at', $month)
                ->get([
                    'vehicles.name as vehicleName',
                    'vehicles.serial_number as serialNumber',
                    'vehicle_models.name as model',
                    'main_vehicle_models.name as mainModel',
                    'spareparts.name as sparepartName',
                    'spareparts.code as sparepartCode',
                    'sparepart_types.name as sparepartType',
                    'companies.name as companyName',
                    'spareparts.quantity',
                    'spareparts.price'
                ])
                ->map(function ($item) use ($year, $month) {
                    return [
                        'year'          => $year,
                        'month'         => $month,
                        'vehicleName'   => $item->vehicleName,
                        'serialNumber'  => $item->serialNumber,
                        'model'         => $item->model,
                        'mainModel'     => $item->mainModel,
                        'sparepartName' => $item->sparepartName,
                        'sparepartCode' => $item->sparepartCode,
                        'sparepartType' => $item->sparepartType,
                        'companyName'   => $item->companyName,
                        'count'         => $item->quantity,
                        'cost'          => $item->quantity * $item->price
                    ];
                });

        foreach ($data as $key => $value) {

            $timeId = $this->addToDimension([
                'year'  => $value['year'],
                'month' => $value['month']
            ], 'date');

            $vehicleId = $this->addToDimension([
                'name'          => $value['vehicleName'],
                'serialNumber'  => $value['serialNumber']
            ], 'vehicle');

            $modelId = $this->addToDimension([
                'name'      => $value['model'],
                'mainModel' => $value['mainModel']
            ], 'model');

            $sparepartId = $this->addToDimension([
                'name'  => $value['sparepartName'],
                'code'  => $value['sparepartCode'],
            ], 'sparepart');

            $sparepartTypeId = $this->addToDimension([
                'name'  => $value['sparepartType'],
            ], 'sparepartType');

            $companyId = $this->addToDimension([
                'name'  => $value['companyName'],
            ], 'company');

            FactMaintenance::create([
                'dim_date_id'           => $timeId,
                'dim_vehicle_id'        => $vehicleId,
                'dim_model_id'          => $modelId,
                'dim_sparepart_id'      => $sparepartId,
                'dim_sparepart_type_id' => $sparepartTypeId,
                'dim_company_id'        => $companyId,
                'count'             => $value['count'],
                'cost'              => $value['cost']
            ]);

        }


    }

    public function addToDimension($data, $dimensionTable) {

        switch ($dimensionTable) {
            case 'date':
                $result = DimDate::where('year', $data['year'])
                                ->where('month', $data['month'])
                                ->get()
                                ->first();
                if (!$result) {
                    $result = DimDate::create([
                        'year'  => $data['year'],
                        'month' => $data['month']
                    ]);
                }

                break;

            case 'vehicle':
                $result = DimVehicle::where('serial_number', $data['serialNumber'])
                                ->get()
                                ->first();
                if (!$result) {
                    $result = DimVehicle::create([
                        'name'          => $data['name'],
                        'serial_number' => $data['serialNumber']
                    ]);
                }

                break;

            case 'model':
                $result = DimModel::where('name', $data['name'])
                                ->where('main_model', $data['mainModel'])
                                ->get()
                                ->first();
                if (!$result) {
                    $result = DimModel::create([
                        'name'       => $data['name'],
                        'main_model' => $data['mainModel']
                    ]);
                }

                break;
            case 'sparepart':
                $result = DimSparepart::where('name', $data['name'])
                                ->where('code', $data['code'])
                                ->get()
                                ->first();
                if (!$result) {
                    $result = DimSparepart::create([
                        'name' => $data['name'],
                        'code' => $data['code']
                    ]);
                }

                break;
            case 'sparepartType':
                $result = DimSparepartType::where('name', $data['name'])
                                ->get()
                                ->first();
                if (!$result) {
                    $result = DimSparepartType::create([
                        'name' => $data['name'],
                    ]);
                }

                break;
            case 'company':
                $result = DimCompany::where('name', $data['name'])
                                ->get()
                                ->first();
                if (!$result) {
                    $result = DimCompany::create([
                        'name' => $data['name'],
                    ]);
                }

                break;

            default:
                # code...
                break;
        }

        return $result->id;
    }

}

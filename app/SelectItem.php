<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectItem extends Model
{
    protected $fillable = ['value', 'attribute_id'];
}

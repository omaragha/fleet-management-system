<?php

Route::prefix('operations')->group(function () {

    Route::get('', 'OperationController@index');
    Route::post('', 'OperationController@create');
    Route::get('{operationId}', 'OperationController@edit');
    Route::put('{operationId}', 'OperationController@update');
    Route::delete('{operationId}', 'OperationController@delete');

});

<?php

Route::prefix('roles')->group(function () {
    Route::get('', 'RoleController@index');
    Route::post('', 'RoleController@create');
    Route::get('{roleId}', 'RoleController@edit');
    Route::put('{roleId}', 'RoleController@update');
    Route::delete('{roleId}', 'RoleController@delete');
    Route::post('role/{roleId}', 'RoleController@assignPrivilegeToRole');    
});
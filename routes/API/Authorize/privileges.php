<?php

Route::prefix('privileges')->group(function () {
    Route::get('', 'PrivilegeController@index');
    Route::post('', 'PrivilegeController@create');
    Route::get('{privilegeId}', 'PrivilegeController@edit');
    Route::put('{privilegeId}', 'PrivilegeController@update');
    Route::delete('{privilegeId}', 'PrivilegeController@delete');    
});

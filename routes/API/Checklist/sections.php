<?php

Route::prefix('sections')->group(function () {

    Route::post('', 'SectionController@create');
    Route::put('{checklistId}', 'SectionController@update');
    Route::delete('{checklistId}', 'SectionController@delete');

});

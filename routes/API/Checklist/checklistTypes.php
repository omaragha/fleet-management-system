<?php

Route::prefix('checklists')->group(function () {

    Route::get('types', 'Checklists\ChecklistTypeController@index');
    Route::post('types', 'Checklists\ChecklistTypeController@create');
    Route::put('types/{checklistTypeId}', 'Checklists\ChecklistTypeController@update');
    
});


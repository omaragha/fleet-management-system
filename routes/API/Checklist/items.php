<?php

Route::prefix('items')->group(function () {
        
    Route::post('', 'SelectItemController@create');
    Route::put('{selectItemId}', 'SelectItemController@update');
    Route::delete('{selectItemId}', 'SelectItemController@delete');

});

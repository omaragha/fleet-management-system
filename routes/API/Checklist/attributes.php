<?php

Route::prefix('attributes')->group(function () {

    Route::post('', 'AttributeController@create');
    Route::get('{attributeId}', 'AttributeController@getAttributeDetails');
    Route::put('{attributeId}', 'AttributeController@update');
    Route::delete('{attributeId}', 'AttributeController@delete');

});

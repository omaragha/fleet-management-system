<?php

Route::prefix('rules')->group(function () {
        
    Route::post('', 'RuleController@create');
    Route::put('{ruleId}', 'RuleController@update');
    Route::delete('{ruleId}', 'RuleController@delete');

});
<?php

Route::prefix('checklists')->group(function () {

    Route::get('', 'Checklists\ChecklistController@index');
    Route::get('{checklistId}', 'Checklists\ChecklistController@getChecklistDetails');
    Route::post('', 'Checklists\ChecklistController@create');
    Route::put('{checklistid}', 'Checklists\ChecklistController@update');
    Route::delete('{checklistid}', 'Checklists\ChecklistController@delete');
    Route::post('template', 'Checklists\ChecklistController@createTemplate');

});

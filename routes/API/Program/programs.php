<?php

Route::prefix('programs')->group(function () {

    Route::get('', 'ServiceProgramController@index');
    Route::post('', 'ServiceProgramController@create');
    Route::get('{serviceProgramId}', 'ServiceProgramController@edit');
    Route::put('{serviceProgramId}', 'ServiceProgramController@update');
    Route::delete('{serviceProgramId}', 'ServiceProgramController@delete');
    Route::get('{serviceProgramId}/start', 'ServiceProgramController@createVisits');

});

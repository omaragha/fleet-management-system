<?php

// Users operations.
Route::prefix('users')->group(function () {
    Route::get('', 'UserController@index');
    Route::post('', 'UserController@create');
    Route::get('assignments','UserController@getAssignments');
    Route::get('{userId}', 'UserController@edit');
    Route::put('{userId}', 'UserController@update');
    Route::delete('{userId}', 'UserController@delete');
    Route::get('{userId}/tasks', 'UserController@getTasksByUser');
    Route::get('{userId}/assignments', 'UserController@getAssignmentsByUser');
});

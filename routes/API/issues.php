<?php

Route::prefix('issues')->group(function () {

    Route::get('', 'IssueController@index');
    Route::post('', 'IssueController@create');
    Route::get('{issueId}', 'IssueController@edit');
    Route::get('solve/{issueId}',"IssueController@solve");
    Route::get('close/{issueId}',"IssueController@close");
    Route::put('{issueId}', 'IssueController@update');
    Route::delete('{issueId}', 'IssueController@delete');

});

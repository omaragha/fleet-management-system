<?php

// Users operations.
Route::prefix('problemReports')->group(function () {
    Route::get('{reportId}/changeStatus', 'ProblemReportController@changeStatus');
    Route::get('', 'ProblemReportController@index');
    Route::post('', 'ProblemReportController@create');
    Route::get('{reportId}', 'ProblemReportController@edit');
    Route::put('{reportId}', 'ProblemReportController@update');
    Route::delete('{reportId}', 'ProblemReportController@delete');
});

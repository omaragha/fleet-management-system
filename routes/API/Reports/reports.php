<?php

Route::prefix('reports')->group(function () {
    Route::get('visits', 'VisitController@visitsReport');
    Route::get('itemFailures', 'ItemFailureController@itemFailuresReport');
    Route::get('issues', 'IssueController@issuesReport');

});

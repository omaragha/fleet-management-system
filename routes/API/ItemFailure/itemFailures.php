<?php

Route::prefix('itemFailures')->group(function () {


    Route::get('{itemFailureId}/details','ItemFailureController@view');
    Route::get('{vehicleId?}', 'ItemFailureController@index');
    Route::put('{itemId}', 'ItemFailureController@update');

});

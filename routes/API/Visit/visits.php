<?php

Route::prefix('visits')->group(function () {

    Route::get('mytasks', 'VisitController@index');
    Route::post('upload', 'VisitController@create');
    Route::get("visitStatus/{visitId}","VisitController@getVisitStatus");
    Route::post('saveValues', 'VisitController@visitVehicle');
    Route::get("{visitId}/rejectTask","VisitController@rejectTask");
    Route::get('calender','VisitController@calender');
    Route::get('{visitId}','VisitController@view');
    Route::get('{visitId}/details', 'VisitController@getVisitDetails');
    Route::get('{visitId}/spareparts', 'VisitController@getSparepartsByVisitId');
    Route::post('{visitId}/finishTask', 'VisitController@finishTask');

});

<?php

Route::prefix('vehicleModels')->group(function () {
    Route::get('', 'VehicleModelController@index');
    Route::post('', 'VehicleModelController@create');
    Route::get('{vehicleModelId}', 'VehicleModelController@edit');
    Route::put('{vehicleModelId}', 'VehicleModelController@update');
    Route::delete('{vehicleModelId}', 'VehicleModelController@delete');
});

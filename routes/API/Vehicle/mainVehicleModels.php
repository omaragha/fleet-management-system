<?php

Route::prefix('mainVehicleModels')->group(function () {
    Route::get('', 'MainVehicleModelController@index');
    Route::post('', 'MainVehicleModelController@create');
    Route::get('{mainVehicleModelId}', 'MainVehicleModelController@edit');
    Route::put('{mainVehicleModelId}', 'MainVehicleModelController@update');
    Route::delete('{mainVehicleModelId}', 'MainVehicleModelController@delete');
    Route::get('{mainVehicleModelId}/companyModels', 'MainVehicleModelController@getCompanyVehicleModelsByMainModel');
});

<?php

Route::prefix('vehicles')->group(function () {

    Route::get('', 'VehicleController@index');
    Route::post('', 'VehicleController@create');
    Route::post("{vehicleId}/startCMVisit",'VehicleController@startCMVisit');
    Route::get("{vehicleId}/visits",'VehicleController@visits');
    
    Route::get("{vehicleId}/getVehicleVisitsDetails",'VehicleController@getVehicleVisitsDetails');
    Route::get('{vehicleId}/failures', 'VehicleController@getItemFailuresByVehicle');
    Route::get('{vehicleId}/changeVehicleStatus', 'VehicleController@changeVehicleStatus');
    Route::get("{vehicleId}/issues",'VehicleController@issues');
    Route::get("{vehicleId}/setProfileImage",'VehicleController@setProfileImage');
    Route::get("{vehicleId}/images",'VehicleController@images');
    Route::post('{vehicleId}/images', 'VehicleController@addImages');
    Route::get('location', 'VehicleController@getLocation');
    Route::put('location', 'VehicleController@updateLocation');
    Route::get('{vehicleId}', 'VehicleController@edit');
    Route::put('{vehicleId}', 'VehicleController@update');
    Route::delete('{vehicleId}', 'VehicleController@delete');




});

<?php

Route::prefix('groups')->group(function () {
    Route::get('', 'GroupController@index');
    Route::post('', 'GroupController@create');
    Route::get('{groupId}', 'GroupController@edit');
    Route::put('{groupId}', 'GroupController@update');
    Route::delete('{groupId}', 'GroupController@delete');
});

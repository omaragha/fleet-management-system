<?php

Route::prefix('watchedVehicles')->group(function () {
    Route::get('', 'WatcehdvehiclesController@index');
    Route::post('', 'WatcehdvehiclesController@create');
    Route::put('{WatcehdvehicleId}', 'WatcehdvehiclesController@update');
    Route::delete('{WatcehdvehicleId}', 'WatcehdvehiclesController@delete');    
});

<?php

Route::prefix('assignments')->group(function () {
    Route::get('', 'AssignmentController@index');
    Route::post('', 'AssignmentController@create');
    Route::get('{assignmentId}', 'AssignmentController@edit');
    Route::get('{assignmentId}/finish', 'AssignmentController@finishAssignment');
    Route::put('{assignmentId}', 'AssignmentController@update');
    Route::delete('{assignmentId}', 'AssignmentController@delete');
});

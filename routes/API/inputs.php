<?php

Route::prefix('inputs')->group(function () {

    Route::get('', 'InputTypeController@index');
    Route::post('', 'InputTypeController@create');
    Route::get('{inputId}', 'InputTypeController@edit');
    Route::put('{inputId}', 'InputTypeController@update');
    Route::delete('{inputId}', 'InputTypeController@delete');

});
<?php

Route::prefix('spareparts')->group(function () {

    Route::get('', 'SparepartController@index');
    Route::post('', 'SparepartController@create');
    Route::get('{sparepartId}', 'SparepartController@edit');
    Route::put('{sparepartId}', 'SparepartController@update');
    Route::delete('{sparepartId}', 'SparepartController@delete');

});

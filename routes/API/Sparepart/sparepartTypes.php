<?php

Route::prefix('sparepartTypes')->group(function () {

    Route::get('', 'SparepartTypeController@index');
    Route::post('', 'SparepartTypeController@create');
    Route::get('{sparepartTypeId}', 'SparepartTypeController@edit');
    Route::put('{sparepartTypeId}', 'SparepartTypeController@update');
    Route::delete('{sparepartTypeId}', 'SparepartTypeController@delete');

});

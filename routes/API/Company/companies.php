<?php

Route::prefix('companies')->group(function () {
    Route::get('', 'CompanyController@index');
    Route::post('', 'CompanyController@create');
    Route::get('{companyId}/activate', 'CompanyController@activate');
    Route::get('{companyId}/deactivate', 'CompanyController@deactivate');
    Route::get('{companyId}', 'CompanyController@edit');
    Route::put('{companyId}', 'CompanyController@update');
    Route::delete('{companyId}', 'CompanyController@delete');
    Route::get('{companyId}/tasks', 'CompanyController@getTasksByCompany');
    Route::get('{companyId}/assignments', 'CompanyController@getAssignmentsByCompany');
    Route::get('{companyId}/itemFailures', 'CompanyController@getMostVehicleFailure');

});

<?php

use Illuminate\Support\Facades\Route;

Route::prefix('companyModels')->group(function () {
    Route::get('', 'CompanyVehicleModelController@index');
    Route::post('', 'CompanyVehicleModelController@create');
    // Route::get('{companyId}', 'companyVehicleModelContCompanyVehicleModelController@edit');
    // Route::put('{companyId}', 'companyVehicleModelContCompanyVehicleModelController@update');
    Route::delete('{companyModelId}', 'CompanyVehicleModelController@delete');
    Route::get('{companyModelId}/checklists', 'CompanyVehicleModelController@checklistsSuggestion');
});

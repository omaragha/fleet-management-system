
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Login route.
include('API\Authentication\login.php');

Route::group(['middleware' => 'auth:api', 'namespace' => 'API'], function () {

    // Register route.
    include('API\Authentication\register.php');

    // Users operations.
    include('API\users.php');

    // Companies operations.
    include('API\Company\companies.php');
    include('API\Company\companyModels.php');

    // Roles operations.
    include('API\Authorize\roles.php');

    // Privileges operations.
    include('API\Authorize\privileges.php');

    // Checklists operations.
    include('API\Checklist\checklistTypes.php');

    include('API\Checklist\checklists.php');



    // Section route.
    include('API\Checklist\sections.php');

    // Attribute route.
    include('API\Checklist\attributes.php');

    // Rule route.
    include('API\Checklist\rules.php');

    // Item route.
    include('API\Checklist\items.php');

    // Operation route.
    include('API\operations.php');

    // Input route.
    include('API\inputs.php');

    // Visit operaitons.
    include('API\Visit\visits.php');

    // justifications.
    include('API\justifications.php');

    //Vehicle Operations
    include('API\Vehicle\vehicles.php');

    //Vehicle Model Operation
    include('API\Vehicle\vehicleModels.php');

    //Assignment Operations
    include('API\Vehicle\assignments.php');

    //Watcehd Vehicles Operations
    include('API\Vehicle\watchedVehicles.php');

    // Sparepart type route.
    include('API\Sparepart\sparepartTypes.php');

    // Sparepart route.
    include('API\Sparepart\spareparts.php');

    // Image route.
    include('API\Image\images.php');

    // Program route.
    include('API\Program\programs.php');

    // Groups route.
    include('API\Vehicle\groups.php');

    // Main Vehicle Models route.
    include('API\Vehicle\mainVehicleModels.php');

    // Item Failure route.
    include('API\ItemFailure\itemFailures.php');

    // Reports route.
    include('API\Reports\reports.php');

    include('API\package.php');

    // Issue route.
    include('API\issues.php');

    // Problem Report route.
    include('API\problemReports.php');

    // Logout route.
    include('API\Authentication\logout.php');

});

